# CHANGELOG for remote repository  https://piwi@github.com/piwi/opensec.git

* v0.2.0 (2015-08-03)

    * 9648e05 - review of completion script + new dump methods in core (piwi)
    * 4b052c1 - large review of configuration variable names (piwi)

* v0.1.0 (2015-08-02 - 2576ea0)

    * Initial release
    