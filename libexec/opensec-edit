#!/usr/bin/env bash
#
# <http://github.com/piwi/opensec>
# Copyright (c) 2015 Pierre Cassat & contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# For sources & updates, see <http://github.com/piwi/opensec>.
#
# To transmit bugs, see <http://github.com/piwi/opensec/issues>.
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code or see <http://www.gnu.org/licenses/>.
#
# opensec-edit: edit a file from a package's data
#
set -e

########## common block ##########
# include required lib
getapplibexec() {
    local cwd="$(pwd)"
    local path="$1"
    while [ -n "$path" ]; do
        cd "${path%/*}" 2>/dev/null; local name="${path##*/}"; path="$($(type -p greadlink readlink | head -1) "$name" || true)";
    done
    pwd
    cd "$cwd"
}
appfile="$(getapplibexec "$0")/opensec-core"
if [ ! -f "$appfile" ]; then echo "> required file '$appfile' not found!" >&2; exit 1; fi
source "$appfile"
########## common block ##########

# custom usage string
usage() {
    echo "Try to open a package's data file in an editor depending on its MIME type."
    echo "Local paths are relative to the 'data/' directory of the package."
    echo
    echo "usage: opensec edit [-v] <package_name> <local_path>"
    echo
    echo "i.e.:  opensec edit /path/to/package /my-file.txt"
    echo "       opensec edit package-name /my-image.png"
}

# common options
common_options

# load package info - required argument
[ "${#ARGUMENTS[@]}" -eq 0 ] && usage && exit 1;
load_package_info "${ARGUMENTS[0]}"
unset 'ARGUMENTS[0]'

# dev
$DEBUG && debug && exit 0;

# process
if [ $PACKAGESTATUS -eq $STATUS_OPENED ]; then

    if [ $PACKAGEDATASTATUS -eq $STATUS_OPENED ]; then

        for target in "${ARGUMENTS[@]}"; do
            targetfull="$(get_package_data_path "$PACKAGEPATH" "$target")"
            [ ! -e "$targetfull" ] && simple_error "path '$target' not found in package (use action 'add' to add it)";
            $VERBOSE && echo "# ${targetfull}"
            log_in_package "$PACKAGEPATH" "opening file $targetfull for edition"
            $APP_EDITOR "$targetfull"
        done

    else
        simple_error "data of package '$PACKAGENAME' are not opened (use action 'extract' to open them)"
    fi

else
    simple_error "package '$PACKAGENAME' is not opened (use action 'extract' to open it)"
fi

exit 0
# Endfile
# vim: autoindent tabstop=4 shiftwidth=4 expandtab softtabstop=4 filetype=sh
