#!/usr/bin/env bash
#
# <http://github.com/piwi/opensec>
# Copyright (c) 2015 Pierre Cassat & contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# For sources & updates, see <http://github.com/piwi/opensec>.
#
# To transmit bugs, see <http://github.com/piwi/opensec/issues>.
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code or see <http://www.gnu.org/licenses/>.
#
# opensec-pack-replace: replace a file into a compressed package
#
set -e

########## common block ##########
# include required lib
getapplibexec() {
    local cwd="$(pwd)"
    local path="$1"
    while [ -n "$path" ]; do
        cd "${path%/*}" 2>/dev/null; local name="${path##*/}"; path="$($(type -p greadlink readlink | head -1) "$name" || true)";
    done
    pwd
    cd "$cwd"
}
appfile="$(getapplibexec "$0")/opensec-core"
if [ ! -f "$appfile" ]; then echo "> required file '$appfile' not found!" >&2; exit 1; fi
source "$appfile"
########## common block ##########

# custom usage string
usage() {
    echo "Replace a file or a directory from a package in any state."
    echo "This will encrypt and compress package and data if needed and rollback after the action."
    echo "Local paths are relative to the 'data/' directory of the package."
    echo
    echo "usage: opensec pack-replace [-v|-f|-c] <package_name> <original_file> <local_path> [data password] [user password]"
    echo
    echo "i.e.:  opensec pack-replace /package/path /file/path/to/add /new/local/name/in/package"
    echo "       opensec pack-replace package-name  ~/file/to/add /new/local/name/in/package"
}

# common options
common_options

# load package info - required argument
[ "${#ARGUMENTS[@]}" -eq 0 ] && usage && exit 1;
load_package_info "${ARGUMENTS[0]}"

# dev
$DEBUG && debug && exit 0;

# process
args=( "$PACKAGEPATH" )
call_subcommand pack-open args[@]
call_subcommand replace ARGUMENTS[@]
call_subcommand pack-close args[@]

exit 0
# Endfile
# vim: autoindent tabstop=4 shiftwidth=4 expandtab softtabstop=4 filetype=sh
