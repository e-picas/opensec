opensec development notes
=========================

OpenSec is built using the "test-driven-programming" concept:

-   a test of a feature is first written and should fail
-   the feature is developed to pass the test

The tests are run with the help of <http://github.com/sstephenson/bats>.
Any test file must be named `*.bats`.

To run the tests, use:

    bats test/*.bats
    # or
    ./test/run-all.sh

Some special tests with a manual action can be run with:

    bats test/*.bats.manual


Todos
-----




Useful links
------------

-   list of builtin Bash variables: <http://www.gnu.org/software/bash/manual/html_node/Bash-Variables.html>
-   the Bash Hackers wiki: <http://wiki.bash-hackers.org/>
-   an online Bash validator: <http://www.shellcheck.net/>
-   a test suite for Bash scripts: <http://github.com/sstephenson/bats>

Work notes
----------

To load local opensec auto-completion, use:

    source etc/bash_completion.d/opensec_completion

To rebuild the manpage:

    markdown-extended --force -f man -o man/opensec.1.man MANPAGE.md

To test its status, use:

    if [ $PACKAGESTATUS -eq $STATUS_TARBALL_ENCRYPTED ]; then
        # stuff on tarball encrypted
    elif [ $PACKAGESTATUS -eq $STATUS_TARBALL ]; then
        # stuff on tarball
    elif [ $PACKAGESTATUS -eq $STATUS_OPENED ]; then
        # stuff on opened
    else
        # ??
    fi

To read a tarball without extracting its contents

    tar -tf path-to-tarball.tar.gz

The full process is something like:

    # make a tarball
    tar -czf DIR.tar.gz dir
    # get its checksum
    $(type -p gmd5sum md5sum | head -1) DIR.tar.gz | cut -d ' ' -f 1 > DIR.tar.gz.sum
    # get password checksum
    echo PASSWORD | $(type -p gsha1sum sha1sum | head -1) - | cut -d ' ' -f 1
    # encrypt it
    openssl enc -aes-256-cbc -a -salt -pass 'pass:PASSWORD' -in DIR.tar.gz -out DIR.tar.gz.enc

BATS test case model:

    @test "[test-REF]: description" {
        run CMD
        $OST_DEBUG && {
            echo "running: CMD"
            echo "output: $output"
            echo "status: $status"
        } >&1
        [ "$status" -eq 0 ]
    }


Sub-commands notes
------------------

### global commands

#### Informational commands

    opensec about
    opensec version
    opensec usage

The "version" command should show the app version information on one line (test appinfo-1).

The "about" command should:

-   show the global copyright information (test appinfo-2)
-   the first line of output must be the "version" output above (test appinfo-2)

The "usage" command should:

-   show the global usage information (simple synopsis - test appinfo-4)
-   the output must have less lines than the global "help" command (test appinfo-4).

#### opensec-help

    opensec help [<sub-command>]
    opensec help
    opensec help status

The "help" command should:

-   show the global help of *opensec* with no argument (test appinfo-3)
-   show the help string of a specific command with command name as argument (test appinfo-5)
-   this should be true for all sub-commands (test appinfo-6)

Each help string should have 3 lines or more, on the following model (test appinfo-6):

    Short string about command action.
    
    usage:  [command synopsis]
    
    i.e.:   [usage example(s)]

#### opensec-config

    opensec config --global
    opensec config --global varname
    opensec config --global varname value
    opensec config <package>
    opensec config <package> varname
    opensec config <package> varname value

The "config" command should:

-   show configuration values loading a `$HOME/.opensec` file if it exists in `global` mode
-   show configuration values loading a `$PACKAGE/.opensec` file if it exists in `local` mode
-   show one single configuration value if a `varname` is defined
-   set a single configuration value, locally or globally, if a `value` is defined

### "init" group

#### opensec-create

    opensec create [-v] <package_name> [optional description]
    opensec create /package/path
    opensec create package-name
    opensec create <package> 'optional package description string'

The "create" command should:

-   create the directory if it does not exist (test init-1)
-   exit with an error if the directory already exists (test init-2)
-   create the default empty structure in it (test init-1)
-   add the description in the package if a description is set (test init-3)

The only option available with this command is `verbose`.

#### opensec-init

       opensec init [-v|-f] <package_name> [optional description]
       opensec init /package/path
       opensec init -f package-name
       opensec init <package> 'optional package description string'

The "init" command should:

-   create the default empty structure in concerned directory if it is empty (test init-4)
-   exit with an error if the directory does not exist (test init-5)
-   create the default structure in concerned directory and add its original contents
    as package's data if it is not empty and:
    -   the user has decided so after a prompt (the process should abort if he answered "no" - manual test init-7)
    -   the `force` option is used (test init-6)
-   add the description in the package if a description is set (test init-8)

The options available with this command are `verbose` and `force`.

### "data" group

#### opensec-list

    opensec list [-v] <package_name>
    opensec list /path/to/package
    opensec list package-name

The "list" command should:

-   list recursively the contents of a package's data directory (test data-1)

The only option available with this command is `verbose`.

#### opensec-show

    opensec show [-v] <package_name> <local_path>
    opensec show /path/to/package /my-file.txt
    opensec show package-name /my-image.png

The "show" command should:

-   try to define the correct MIME type of concerned local path (test data-2 & data-3)
-   open the path in the appropriate visualizer (test data-2 & data-3)

The only option available with this command is `verbose`.

#### opensec-edit

    opensec edit [-v] <package_name> <local_path>
    opensec edit /path/to/package /my-file.txt
    opensec edit package-name /my-image.png

The "edit" command should:

-   try to define the correct MIME type of concerned local path (test data-4 & data-5)
-   open the path in the appropriate editor (test data-4 & data-5)

The only option available with this command is `verbose`.

#### opensec-add

    opensec add [-v] <package_name> <original_file> [local_path=/]
    opensec add /package/path /file/path/to/add
    opensec add package-name ~/file/to/add /new/local/name/in/package

The "add" command should:

-   add concerned path (file or directory), recursively (test data-11), in the package:
    -   at the root of the package's data if no local path is set (test data-6)
    -   if a local path is set (<output> in the followings):
        -   if the output is an existing directory in package's data, the input 
            should be added in that directory (test data-8)
        -   if the output is not an existing path in package's data, the input
            should be renamed to the output (test data-7)
-   exit with an error if the input path does not exist (test data-9)
-   exit with an error if the output path already exists (test data-10)

The only option available with this command is `verbose`.

#### opensec-remove

    opensec remove [-v|-f] <package_name> <local_path>
    opensec remove /package/path /local/path/to/remove
    opensec remove -f package-name /local/path/to/remove

The "remove" command should:

-   remove the path set as argument (test data-12), recursively if it is a directory (test data-13)
-   the delete process should be interactive by default (with user confirmation - test data-manual)
    except in "forced" mode (test data-12 & data-13)
-   the command should through an error if the path does not exist (test data-14)

The options available with this command are `verbose` and `force`.

#### opensec-replace

    opensec replace [-v] <package_name> <original_file> <local_path>
    opensec replace /package/path /file/path/to/add /new/local/name/in/package
    opensec replace package-name ~/file/to/add /new/local/name/in/package

The "replace" command should be a shortcut of the following process:

    opensec remove ... && opensec add ...

### "packing" group

#### opensec-compress & opensec-compress-data

    opensec compress [-v] <package_name>
    opensec compress-data [-v] <package_name>
    opensec compress /package/path
    opensec compress-data package-name

The "compress" command should:

-   make a tarball of concerned input (a package or its data) if it is
    opened and calculate the sum of the resulting tarball to write it
    in a checksum file
-   do nothing and inform user if concerned tarball already exists
-   end with an error if concerned path is an encrypted tarball

#### opensec-extract & opensec-extract-data

    opensec extract [-v] <package_name>
    opensec extract-data [-v] <package_name>
    opensec extract /package/path.tar.gz
    opensec extract /package/path
    opensec extract /package/path/data
    opensec extract-data /package/path
    opensec extract-data package-name

The "extract" command should:

-   open a tarball of concerned input (a package or its data) if it is
    in that state
-   before to extract the tarball, if a checksum file exist for this archive,
    the sum should be verified ; an information may be written to inform user
    only in case of data corruption
-   do nothing and inform user if concerned opened directory already exists
-   end with an error if concerned path is an encrypted tarball

#### opensec-encrypt & opensec-encrypt-data

    opensec encrypt [-v] <package_name> [password]
    opensec encrypt-data [-v] <package_name> [password]
    opensec encrypt /package/path
    opensec encrypt /package/path.tar.gz 'my password'
    opensec encrypt /package/path.tar.gz.enc
    opensec encrypt-data package-name 'my password'

The "encrypt" command should:

-   ask user for a password clue except in `forced` mode
-   ask user for the password to use if it is not set as argument
-   encrypt concerned input (a package or its data) if it is a tarball
-   do nothing and inform user if concerned encrypted file already exists
-   end with an error if concerned path is an opened directory (not compressed)

#### opensec-decrypt & opensec-decrypt-data

    opensec decrypt [-v] <package_name> [password]
    opensec decrypt-data [-v] <package_name> [password]
    opensec decrypt /package/path
    opensec decrypt /package/path.tar.gz 'my password'
    opensec decrypt /package/path.tar.gz.enc
    opensec decrypt-data package-name 'my password'

The "decrypt" command should:

-   decrypt concerned input (a package or its data) if it is encrypted
-   do nothing and inform user if concerned tarball file already exists (is not encrypted)
-   end with an error if concerned path is an opened directory (not compressed neither encrypted)

#### opensec-open & opensec-open-data

    opensec open [-v|-f] <package_name> [password]
    opensec open /package/path
    opensec open /package/path.tar.gz 'my password'
    opensec open /package/path.tar.gz.enc
    opensec open package-name 'user'

The "open" command should be a shortcut to:

    if [ is_encrypted input ] opensec decrypt ...
    if [ is_compressed input ] opensec extract ...

#### opensec-close & opensec-close-data

    opensec close [-v|-f|-c] <package_name> [password]
    opensec close /package/path
    opensec close /package/path 'my password'
    opensec close package-name 'user'

The "close" command should be a shortcut to:

    if [ ! is_compressed input ] opensec compress ...
    if [ ! is_encrypted input ] opensec encrypt ...

### "meta" group

#### opensec-status

    opensec status [-v] <package_name>
    opensec status /package/path
    opensec status package-name

The "status" command should:

-   inform user about the state (opened, compressed or encrypted) of
    a package and its data
-   inform user about the size of a package and its data
-   inform user about data integrity if possible

#### opensec-log

    opensec log [-v] <package_name> ['message']
    opensec log /package/path
    opensec log package-name 'my new log line'

The "log" command should:

-   show the log contents of a package with no argument
-   add a new log entry with a message argument

#### opensec-description

    opensec description [-v] <package_name> [edit OR prompt OR 'message']
    opensec description /package/path
    opensec description /package/path edit
    opensec description package-name 'my description'

The "description" command should:

-   show the description of a package with no argument
-   replace the actual description with an argument:
    -   opening the clue file in `$APP_EDITOR` with argument `edit`
    -   with the raw argument in any other cases

#### opensec-clue

    opensec clue [-v] <package_name> [edit OR prompt OR 'message']
    opensec clue /package/path
    opensec clue /package/path edit
    opensec clue package-name 'my password clue'
    opensec clue package-name prompt

The "clue" command should:

-   show the password's clue of a package with no argument
-   replace the actual password's clue with an argument:
    -   opening the clue file in `$APP_EDITOR` with argument `edit`
    -   by a user prompt with argument `prompt`
    -   with the raw argument in any other cases

#### opensec-check

    opensec check [-v] <package_name>
    opensec check /package/path
    opensec check package-name

The "check" command should:

-   validate tarball integrity of the whole package if it is compressed
    and a sum file exists
-   validate tarball integrity of a package's data if it is compressed
    and a sum file exists
-   end with user information in any other cases

The validation process should:

-   write an information on STDOUT if tarball is valid
-   write an warning on STDERR if tarball is not valid
-   never exit with a non-zero status except in `verbose` mode

### "packed" group

#### opensec-pack-open

    opensec pack-open [-v|-f] <package_name> [data password] [user password]
    opensec pack-open /package/path
    opensec pack-open /package/path.tar.gz 'my password'
    opensec pack-open /package/path.tar.gz.enc
    opensec pack-open package-name 'my password' 'user'

The "pack-open" command should be a shortcut to:

    if [ package_is_encrypted input ] opensec decrypt ...
    if [ package_is_compressed input ] opensec extract ...
    if [ data_is_encrypted input ] opensec decrypt-data ...
    if [ data_is_compressed input ] opensec extract-data ...

#### opensec-pack-close

    opensec pack-close [-v|-f|-c] <package_name> [data password] [user password]
    opensec pack-close /package/path
    opensec pack-close /package/path 'my password'
    opensec pack-close package-name 'my password' 'user'

The "pack-close" command should be a shortcut to:

    if [ ! data_is_compressed input ] opensec compress-data ...
    if [ ! data_is_encrypted input ] opensec encrypt-data ...
    if [ ! package_is_compressed input ] opensec compress ...
    if [ ! package_is_encrypted input ] opensec encrypt ...

#### opensec-pack-* with all "data" commands

A "pack-*" command should be a shortcut to:

    opensec-pack-open
    opensec-*
    opensec-pack-close

The concept is to open a package, do something on it (with one of the "data" subcommands)
and then rebuild the package in its original state.
