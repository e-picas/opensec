#!/usr/bin/env bash
#
# <http://github.com/piwi/opensec>
# Copyright (c) 2015 Pierre Cassat & contributors
# License GNU-GPL v 3.0 - This program comes with ABSOLUTELY NO WARRANTY.
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code or see <http://www.gnu.org/licenses/>.
#
# Run all *.bats files with BATS
#

here="$(dirname "$0")"
batscmd="${1:-bats}"
status=0

for f in ${here}/*.bats; do
    echo "> running: $f"
    "$batscmd" "$f"
    let "status=status+$?"
done

exit $status
