#!/usr/bin/env bats
#
# <http://github.com/piwi/opensec>
# Copyright (c) 2015 Pierre Cassat & contributors
# License GNU-GPL v 3.0 - This program comes with ABSOLUTELY NO WARRANTY.
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code or see <http://www.gnu.org/licenses/>.
#
# Manual tests
#

load test-commons

setup() { tests_setup; }

teardown() { tests_teardown; }

@test "[data-3]: show content of an image file" {
    skip 'TODO'
}

@test "[data-5]: edit content of a text file" {
    skip 'TODO'
    [ -d "$OST_TMP_PACKAGE" ] && rm -rf "$OST_TMP_PACKAGE";
    [ ! -d "$OST_TMP_PACKAGE" ] && mkdir "$OST_TMP_PACKAGE";
    [ -d "$OST_TMP_PACKAGE" ]
    cp -r "$OST_FIXTURES_PACKAGEDIR_OPENED"/* "$OST_TMP_PACKAGE"
    run "$OPENSECLIBEXEC/opensec" edit "$OST_TMP_PACKAGE" "$OST_CONTENTS_FILE" <<'MSG'
:s/At vero/test/
:wq
MSG
    loremipsumtestfile="$(get_package_data_path "$OST_FIXTURES_PACKAGEDIR_OPENED" "$OST_CONTENTS_FILE")"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec edit $OST_TMP_PACKAGE $OST_CONTENTS_FILE"
        echo "output: $output"
        echo "status: $status"
        echo "sould be: $(cat "$loremipsumtestfile")"
    } >&1
    [[ "$output" = "test"* ]]
}

@test "[data-6]: edit content of an image file" {
    skip 'TODO'
}

@test "[packing-5]: encryption of package's compressed data and no FORCED mode" {
    make_opened_package;
    # compress data
    run "$OPENSECLIBEXEC/opensec" compress-data "$OST_TMP_PACKAGE"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec compress-data $OST_TMP_PACKAGE"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ -f "${OST_TMP_PACKAGE}/${DIR_DATA}${EXT_TARBALL}" ]
    [ -f "${OST_TMP_PACKAGE}/${DIR_DATA}${EXT_TARBALL}${EXT_SUM}" ]
    # encrypt data
    run "$OPENSECLIBEXEC/opensec" encrypt-data "$OST_TMP_PACKAGE" <<MSG
${OST_CLUE}
${OST_PASS}
MSG
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec encrypt-data $OST_TMP_PACKAGE"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ -f "${OST_TMP_PACKAGE}/${DIR_DATA}${EXT_TARBALL}${EXT_CRYPTED}" ]
    [ ! -f "${OST_TMP_PACKAGE}/${DIR_DATA}${EXT_TARBALL}" ]
    [ -f "${OST_TMP_PACKAGE}/${DIR_DATA}${EXT_TARBALL}${EXT_SUM}" ]
}

@test "[packing-9]: full close of package's data and no FORCED mode" {
    make_opened_package;
    run "$OPENSECLIBEXEC/opensec" close-data "$OST_TMP_PACKAGE" <<MSG
${OST_CLUE}
${OST_PASS}
MSG
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec close-data $OST_TMP_PACKAGE"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ -f "${OST_TMP_PACKAGE}/${DIR_DATA}${EXT_TARBALL}${EXT_CRYPTED}" ]
    [ ! -f "${OST_TMP_PACKAGE}/${DIR_DATA}${EXT_TARBALL}" ]
    [ -f "${OST_TMP_PACKAGE}/${DIR_DATA}${EXT_TARBALL}${EXT_SUM}" ]
}

@test "[packing-14]: encryption of compressed package and no FORCED mode" {
    make_opened_package;
    # close data
    run "$OPENSECLIBEXEC/opensec" close-data "$OST_TMP_PACKAGE" <<MSG
${OST_CLUE}
${OST_PASS}
MSG
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec close-data $OST_TMP_PACKAGE"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ -f "${OST_TMP_PACKAGE}/${DIR_DATA}${EXT_TARBALL}${EXT_CRYPTED}" ]
    [ ! -f "${OST_TMP_PACKAGE}/${DIR_DATA}${EXT_TARBALL}" ]
    [ -f "${OST_TMP_PACKAGE}/${DIR_DATA}${EXT_TARBALL}${EXT_SUM}" ]
    # compress package
    run "$OPENSECLIBEXEC/opensec" compress "$OST_TMP_PACKAGE"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec compress $OST_TMP_PACKAGE"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ -f "${OST_TMP_PACKAGE}${EXT_TARBALL}" ]
    [ -f "${OST_TMP_PACKAGE}${EXT_TARBALL}${EXT_SUM}" ]
    # encrypt package
    run "$OPENSECLIBEXEC/opensec" encrypt "$OST_TMP_PACKAGE" <<MSG
${OST_USER}
MSG
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec encrypt $OST_TMP_PACKAGE"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ -f "${OST_TMP_PACKAGE}${EXT_TARBALL}${EXT_CRYPTED}" ]
    [ ! -f "${OST_TMP_PACKAGE}${EXT_TARBALL}" ]
    [ -f "${OST_TMP_PACKAGE}${EXT_TARBALL}${EXT_SUM}" ]
}

@test "[packing-18]: full close of a package and no FORCED mode" {
    make_opened_package;
    # close data
    run "$OPENSECLIBEXEC/opensec" close-data "$OST_TMP_PACKAGE" <<MSG
${OST_CLUE}
${OST_PASS}
MSG
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec close-data $OST_TMP_PACKAGE"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ -f "${OST_TMP_PACKAGE}/${DIR_DATA}${EXT_TARBALL}${EXT_CRYPTED}" ]
    [ ! -f "${OST_TMP_PACKAGE}/${DIR_DATA}${EXT_TARBALL}" ]
    [ -f "${OST_TMP_PACKAGE}/${DIR_DATA}${EXT_TARBALL}${EXT_SUM}" ]
    # close package
    run "$OPENSECLIBEXEC/opensec" -c close "$OST_TMP_PACKAGE" <<MSG
${OST_USER}
MSG
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec -c close $OST_TMP_PACKAGE"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ -f "${OST_TMP_PACKAGE}${EXT_TARBALL}${EXT_CRYPTED}" ]
    [ ! -f "${OST_TMP_PACKAGE}${EXT_TARBALL}" ]
    [ -f "${OST_TMP_PACKAGE}${EXT_TARBALL}${EXT_SUM}" ]
}

@test "[packing-101]: decrypt of encrypted package and no FORCED mode" {
    make_closed_package_encrypted;
    package="${OST_TESTDIR}/$(basename "$OST_FIXTURES_PACKAGEDIR_ENCRYPTED")"
    run "$OPENSECLIBEXEC/opensec" decrypt "$package" <<MSG
${OST_USER}
MSG
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec decrypt $package"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ -f "${package/${EXT_CRYPTED}/}" ]
}

@test "[packing-105]: uncompress of compressed package and no FORCED mode" {
    make_closed_package_encrypted;
    # decrypt package
    package="${OST_TESTDIR}/$(basename "$OST_FIXTURES_PACKAGEDIR_ENCRYPTED")"
    run "$OPENSECLIBEXEC/opensec" decrypt "$package" <<MSG
${OST_USER}
MSG
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec decrypt $package"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ -f "${package/${EXT_CRYPTED}/}" ]
    # uncompress it
    package="${package/${EXT_CRYPTED}/}"
    [ -f "${package}${EXT_SUM}" ]
    run "$OPENSECLIBEXEC/opensec" extract "$package"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec extract $package"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ -d "${package/${EXT_TARBALL}/}" ]
    [ -f "${package/${EXT_TARBALL}/}/${DIR_DATA}${EXT_TARBALL}${EXT_CRYPTED}" ]
    [ -f "${package/${EXT_TARBALL}/}/${DIR_DATA}${EXT_TARBALL}${EXT_SUM}" ]
}

@test "[packing-109]: open of compressed and encrypted package and no FORCED mode" {
    make_closed_package_encrypted;
    package="${OST_TESTDIR}/$(basename "$OST_FIXTURES_PACKAGEDIR_ENCRYPTED")"
    run "$OPENSECLIBEXEC/opensec" open "$package" <<MSG
${OST_USER}
MSG
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec open $package"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    package="${package/${EXT_CRYPTED}/}"
    [ -d "${package/${EXT_TARBALL}/}" ]
    [ -f "${package/${EXT_TARBALL}/}/${DIR_DATA}${EXT_TARBALL}${EXT_CRYPTED}" ]
    [ -f "${package/${EXT_TARBALL}/}/${DIR_DATA}${EXT_TARBALL}${EXT_SUM}" ]
}

@test "[packing-111]: decrypt of encrypted and compressed package's data and no FORCED mode" {
    make_closed_package_encrypted;
    # open package
    package="${OST_TESTDIR}/$(basename "$OST_FIXTURES_PACKAGEDIR_ENCRYPTED")"
    run_ "$OPENSECLIBEXEC/opensec" open "$package" <<MSG
${OST_USER}
MSG
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec open $package"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    package="${package/${EXT_CRYPTED}/}"
    package="${package/${EXT_TARBALL}/}"
    [ -d "${package}" ]
    [ -f "${package}/${DIR_DATA}${EXT_TARBALL}${EXT_CRYPTED}" ]
    [ -f "${package}/${DIR_DATA}${EXT_TARBALL}${EXT_SUM}" ]
    # decrypt data
    run "$OPENSECLIBEXEC/opensec" decrypt-data "$package" <<MSG
${OST_PASS}
MSG
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec decrypt-data $package"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ -f "${package}/${DIR_DATA}${EXT_TARBALL}" ]
}

@test "[packing-115]: extact of compressed package's data and no FORCED mode" {
    make_closed_package_encrypted;
    # open package
    package="${OST_TESTDIR}/$(basename "$OST_FIXTURES_PACKAGEDIR_ENCRYPTED")"
    run "$OPENSECLIBEXEC/opensec" open "$package" <<MSG
${OST_USER}
MSG
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec open $package"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    package="${package/${EXT_CRYPTED}/}"
    package="${package/${EXT_TARBALL}/}"
    [ -d "${package}" ]
    [ -f "${package}/${DIR_DATA}${EXT_TARBALL}${EXT_CRYPTED}" ]
    [ -f "${package}/${DIR_DATA}${EXT_TARBALL}${EXT_SUM}" ]
    # decrypt data
    run "$OPENSECLIBEXEC/opensec" decrypt-data "$package" <<MSG
${OST_PASS}
MSG
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec decrypt-data $package"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ -f "${package}/${DIR_DATA}${EXT_TARBALL}" ]
    # uncompress it
    run "$OPENSECLIBEXEC/opensec" extract-data "$package"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec extract-data $package"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ -d "${package}/${DIR_DATA}" ]
}

@test "[packing-119]: open of compressed and encrypted package's data and no FORCED mode" {
    make_closed_package_encrypted;
    # open package
    package="${OST_TESTDIR}/$(basename "$OST_FIXTURES_PACKAGEDIR_ENCRYPTED")"
    run "$OPENSECLIBEXEC/opensec" open "$package" <<MSG
${OST_PASS}
MSG
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec open $package"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    package="${package/${EXT_CRYPTED}/}"
    package="${package/${EXT_TARBALL}/}"
    [ -d "${package}" ]
    [ -f "${package}/${DIR_DATA}${EXT_TARBALL}${EXT_CRYPTED}" ]
    [ -f "${package}/${DIR_DATA}${EXT_TARBALL}${EXT_SUM}" ]
    # open data
    run "$OPENSECLIBEXEC/opensec" open-data "$package" <<MSG
${OST_PASS}
MSG
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec open-data $package"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ -d "${package}/${DIR_DATA}" ]
}

@test "[meta-7]: test of description edit" {
    skip 'test'
    make_opened_package
    descile="$(get_package_description_path "$OST_TMP_PACKAGE")"
    [ -f "$descile" ]
    [ "$(strip_empty_lines "$(cat "$descfile")")" = "$OST_DESC" ]
    run "$OPENSECLIBEXEC/opensec" description "$OST_TMP_PACKAGE" edit
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec description $OST_TMP_PACKAGE edit"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -ne 0 ]
    [ -n "$output" ]
}

@test "[meta-10]: test of clue edit" {
    skip 'test'
    make_opened_package
    cluefile="$(get_package_clue_path "$OST_TMP_PACKAGE")"
    [ -f "$cluefile" ]
    [ "$(strip_empty_lines "$(cat "$cluefile")")" = "$OST_CLUE" ]
    run "$OPENSECLIBEXEC/opensec" clue "$OST_TMP_PACKAGE" edit
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec clue $OST_TMP_PACKAGE edit"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -ne 0 ]
    [ -n "$output" ]
}

@test "[meta-11]: test of clue prompt" {
    skip 'test'
    make_opened_package
    cluefile="$(get_package_clue_path "$OST_TMP_PACKAGE")"
    [ -f "$cluefile" ]
    [ "$(strip_empty_lines "$(cat "$cluefile")")" = "$OST_CLUE" ]
    run "$OPENSECLIBEXEC/opensec" clue "$OST_TMP_PACKAGE" prompt
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec clue $OST_TMP_PACKAGE prompt"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -ne 0 ]
    [ -n "$output" ]
}

@test "[packed-2]: full close of an opened package and no FORCED mode" {
    make_opened_package
    run "$OPENSECLIBEXEC/opensec" -c pack-close "$OST_TMP_PACKAGE" <<MSG
${OST_PASS}
${OST_USER}
MSG
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec -c pack-close $OST_TMP_PACKAGE"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ -f "${OST_TMP_PACKAGE}${EXT_TARBALL}${EXT_CRYPTED}" ]
    [ ! -f "${OST_TMP_PACKAGE}${EXT_TARBALL}" ]
    [ -f "${OST_TMP_PACKAGE}${EXT_TARBALL}${EXT_SUM}" ]
}

@test "[packed-4]: full open of a closed package and no FORCED mode" {
    make_closed_package_compressed;
    package="${OST_TESTDIR}/$(basename "$OST_FIXTURES_PACKAGEDIR_TARBALL")"
    run "$OPENSECLIBEXEC/opensec" pack-open "$package" <<MSG
${OST_PASS}
MSG
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec pack-open $package"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    package="${package/${EXT_CRYPTED}/}"
    package="${package/${EXT_TARBALL}/}"
    [ -d "${package}/${DIR_DATA}" ]
}

@test "[packed-6]: add a simple file and no FORCED mode" {
    make_closed_package_compressed;
    package="${OST_TESTDIR}/$(basename "$OST_FIXTURES_PACKAGEDIR_TARBALL")"
    original="${OST_FIXTURES_LOREMIPSUMDIR}/${OST_CONTENTS_FILE}"
    [ -f "$original" ]
    sumfile="${OST_FIXTURES_PACKAGEDIR_TARBALL}${EXT_SUM}"
    [ -f "$sumfile" ]
    begin="$(cat "$sumfile")"
    run "$OPENSECLIBEXEC/opensec" pack-add "$package" "$original" <<MSG
${OST_PASS}
MSG
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec pack-add $package $original"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    end="$(cat "$sumfile")"
    [ "begin" != "$end" ]
}
