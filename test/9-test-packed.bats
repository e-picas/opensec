#!/usr/bin/env bats
#
# <http://github.com/piwi/opensec>
# Copyright (c) 2015 Pierre Cassat & contributors
# License GNU-GPL v 3.0 - This program comes with ABSOLUTELY NO WARRANTY.
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code or see <http://www.gnu.org/licenses/>.
#
# Test for "meta" commands: status / log / clue / description / check
#

load test-commons

setup(){ tests_setup; }

teardown(){ tests_teardown; }

@test "[packed-1]: full close of an opened package" {
    make_opened_package
    run "$OPENSECLIBEXEC/opensec" -f -c pack-close "$OST_TMP_PACKAGE" "$OST_PASS" "$OST_USER"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec -f -c pack-close $OST_TMP_PACKAGE $OST_PASS $OST_USER"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ -f "${OST_TMP_PACKAGE}${EXT_TARBALL}${EXT_CRYPTED}" ]
    [ ! -f "${OST_TMP_PACKAGE}${EXT_TARBALL}" ]
    [ -f "${OST_TMP_PACKAGE}${EXT_TARBALL}${EXT_SUM}" ]
}

@test "[packed-2]: full close of an opened package and no FORCED mode" {
    skip 'MANUAL'
}

@test "[packed-3]: full open of a closed package" {
    make_closed_package_compressed;
    package="${OST_TESTDIR}/$(basename "$OST_FIXTURES_PACKAGEDIR_TARBALL")"
    run "$OPENSECLIBEXEC/opensec" -f pack-open "$package" "$OST_PASS" "$OST_USER"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec -f pack-open $package $OST_PASS $OST_USER"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    package="${package/${EXT_CRYPTED}/}"
    package="${package/${EXT_TARBALL}/}"
    [ -d "${package}/${DIR_DATA}" ]
}

@test "[packed-4]: full open of a closed package and no FORCED mode" {
    skip 'MANUAL'
}

@test "[packed-5]: add a simple file" {
    skip 'TO REWRITE'
    make_closed_package_compressed;
    package="${OST_TESTDIR}/$(basename "$OST_FIXTURES_PACKAGEDIR_TARBALL")"
    original="${OST_FIXTURES_LOREMIPSUMDIR}/${OST_CONTENTS_FILE}"
    [ -f "$original" ]
    sumfile="${OST_FIXTURES_PACKAGEDIR_TARBALL}${EXT_SUM}"
    [ -f "$sumfile" ]
    begin="$(cat "$sumfile")"
    run "$OPENSECLIBEXEC/opensec" -f pack-add "$package" "$original" "$OST_PASS" "$OST_USER"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec -f pack-add $package $original $OST_PASS $OST_USER"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    end="$(cat "$sumfile")"
    [ "begin" != "$end" ]
}

@test "[packed-6]: add a simple file and no FORCED mode" {
    skip 'MANUAL'
}
