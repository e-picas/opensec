#!/usr/bin/env bash
#
# <http://github.com/piwi/opensec>
# Copyright (c) 2015 Pierre Cassat & contributors
# License GNU-GPL v 3.0 - This program comes with ABSOLUTELY NO WARRANTY.
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code or see <http://www.gnu.org/licenses/>.
#
# Common library for BATS tests
# Variables are prefixed by `OST_` ("OpenSec Tests")
#

# debug tests ?
OST_DEBUG=true

# temp test dir
OST_TESTDIR="$(mktemp -dt "opensec.XXXXXX")"
OST_TMP_PACKAGE="${OST_TESTDIR}/testpackage"

# test strings
OST_DESC='my test description'
OST_CLUE='my password clue'
OST_PASS='test'
OST_USER='username'
OST_USER_SHA1='407fefd3833138100041ba17baddb71ba09d35f8'
OST_LOREM='lorem ipsum'

# internal fixtures
OST_FIXTURESDIR="$(pwd)/test/fixtures/"
OST_CONTENTS_FILE="test.txt"
OST_CONTENTS_FILEBIS="test-bis.txt"
OST_CONTENTS_IMAGE="test.jpg"
OST_CONTENTS_PDF="test.pdf"
OST_CONTENTS_ODT="test.odt"
OST_CONTENTS_SUBDIR="test-subdir"
OST_CONTENTS_SUBDIRBIS="test-subdir-bis"
OST_FIXTURES_LOREMIPSUMDIR="${OST_FIXTURESDIR}/lorem-ipsum"
OST_FIXTURES_PACKAGEDIR_EMPTY="${OST_FIXTURESDIR}/lorem-ipsum-package-empty"
OST_FIXTURES_PACKAGEDIR_OPENED="${OST_FIXTURESDIR}/lorem-ipsum-package"
OST_FIXTURES_PACKAGEDIR_DATATARBALL="${OST_FIXTURESDIR}/lorem-ipsum-package-data-tarball"
OST_FIXTURES_PACKAGEDIR_DATAENCRYPTED="${OST_FIXTURESDIR}/lorem-ipsum-package-data-encrypted"
OST_FIXTURES_PACKAGEDIR_TARBALL="${OST_FIXTURESDIR}/lorem-ipsum-package-tarball.tar.gz"
OST_FIXTURES_PACKAGEDIR_TARBALLSUM="${OST_FIXTURESDIR}/lorem-ipsum-package-tarball.tar.gz.sum"
OST_FIXTURES_PACKAGEDIR_ENCRYPTED="${OST_FIXTURESDIR}/lorem-ipsum-package-tarball.tar.gz.enc"
OST_FIXTURES_CONFIG="${OST_FIXTURESDIR}/.opensec"

# internal libexec
OPENSECLIBEXEC="$(pwd)/libexec/"
if [ ! -d "${OPENSECLIBEXEC}" ]; then
    echo "> can not find the 'libexec/' directory (guessed '${OPENSECLIBEXEC}' not found)!" >&2
    exit 1
fi

# requried lib
if [ ! -f "${OPENSECLIBEXEC}/opensec-core" ]; then
    echo "> required file '${OPENSECLIBEXEC}/opensec-core' not found!" >&2
    exit 1
fi
source "${OPENSECLIBEXEC}/opensec-core"

# library for BATS tests

# get last m time of a file
get_last_modification_time()
{
    echo "$(expr "$(date +%s)" - "$(stat -c %Y "$1")" )"
}

# delete empty lines from a content
strip_empty_lines()
{
    echo "${1}" | sed '/^$/d'
}

# get the number of lines of a content
get_number_of_lines()
{
    echo "$(wc -l < "$1")"
}

# prepare the temporary package
tests_setup()
{
    [ -d "$OST_TMP_PACKAGE" ] && rm -rf "$OST_TMP_PACKAGE";
    [ ! -d "$OST_TMP_PACKAGE" ] && mkdir "$OST_TMP_PACKAGE";
    [ -d "$OST_TMP_PACKAGE" ] || exit 1;
}

# cleanup the temporary package
tests_teardown()
{
    [ -d "$OST_TMP_PACKAGE" ] && rm -rf "$OST_TMP_PACKAGE" || { echo '' >/dev/null; };
}

# cleanup temporary package contents
cleanup_package_contents()
{
    [ -d "$OST_TMP_PACKAGE" ] && rm -rf "${OST_TMP_PACKAGE}/*" || { echo '' >/dev/null; };
}

# cleanup temporary directory
cleanup()
{
    local tmpdir="$(dirname "${OST_TESTDIR}")"
    [ -n "${tmpdir}" ] && [ -d "${tmpdir}" ] && rm -rf "${tmpdir:?}/opensec*";
}

# copy a closed package in temporary directory
make_closed_package_encrypted()
{
    cp -f "$OST_FIXTURES_PACKAGEDIR_ENCRYPTED" "${OST_TESTDIR}/"
    cp -f "$OST_FIXTURES_PACKAGEDIR_TARBALLSUM" "${OST_TESTDIR}/"
}

# copy a compressed (not encrypted) package in temporary directory
make_closed_package_compressed()
{
    cp -f "$OST_FIXTURES_PACKAGEDIR_TARBALL" "${OST_TESTDIR}/"
    cp -f "$OST_FIXTURES_PACKAGEDIR_TARBALLSUM" "${OST_TESTDIR}/"
}

# copy a opened package with closed data in temporary directory
make_opened_package_data_encrypted()
{
    cp -rf "$OST_FIXTURES_PACKAGEDIR_DATAENCRYPTED" "${OST_TESTDIR}/"
}

# copy a opened package with compressed (not encrypted) data in temporary directory
make_opened_package_data_compressed()
{
    cp -rf "$OST_FIXTURES_PACKAGEDIR_DATATARBALL" "${OST_TESTDIR}/"
}

# copy a fully opened package with lorem-ipsum data in temporary directory
make_opened_package_fullyopened()
{
    cp -rf "$OST_FIXTURES_PACKAGEDIR_OPENED" "${OST_TESTDIR}/"
}

# copy a fully opened package with lorem-ipsum data in temporary directory
make_opened_package()
{
    cp -rf "$OST_FIXTURES_PACKAGEDIR_OPENED"/* "$OST_TMP_PACKAGE"
}

# copy an empty fully opened package in temporary directory
make_empty_package()
{
    cp -rf "$OST_FIXTURES_PACKAGEDIR_EMPTY"/* "$OST_TMP_PACKAGE"
}

# copy the lorem-ipsum directory in temporary directory
make_no_package()
{
    cp -rf "$OST_FIXTURES_LOREMIPSUMDIR"/* "$OST_TMP_PACKAGE"
}
