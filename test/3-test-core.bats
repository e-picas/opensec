#!/usr/bin/env bats
#
# <http://github.com/piwi/opensec>
# Copyright (c) 2015 Pierre Cassat & contributors
# License GNU-GPL v 3.0 - This program comes with ABSOLUTELY NO WARRANTY.
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code or see <http://www.gnu.org/licenses/>.
#
# Test for the core or opensec
#

load test-commons

@test "[core-1]: resolve_link" {
    # no arg error
    run resolve_link
    $OST_DEBUG && {
        echo "running: resolve_link"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -ne 0 ]
    # classic
    binpath="$(pwd)/bin/opensec"
    run resolve_link "$binpath"
    $OST_DEBUG && {
        echo "running: resolve_link $binpath"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ "$output" = "../libexec/opensec" ]
}

@test "[core-2]: abs_dirname" {
    # no arg error
    run abs_dirname
    $OST_DEBUG && {
        echo "running: abs_dirname"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -ne 0 ]
    # classic
    binpath="$(pwd)/bin/opensec"
    run abs_dirname "$binpath"
    $OST_DEBUG && {
        echo "running: abs_dirname $binpath"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ "$output" = "$(pwd)/libexec" ]
}

@test "[core-3]: strip_trailing_slash" {
    # no arg error
    run strip_trailing_slash
    $OST_DEBUG && {
        echo "running: strip_trailing_slash"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -ne 0 ]
    # classic
    binpath="$(pwd)/"
    run strip_trailing_slash "$binpath"
    $OST_DEBUG && {
        echo "running: strip_trailing_slash $binpath"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ "$output" = "$(pwd)" ]
}

@test "[core-4]: get_line_number_matching" {
    # no arg error
    run get_line_number_matching
    $OST_DEBUG && {
        echo "running: get_line_number_matching"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -ne 0 ]
    # path error
    run get_line_number_matching "$(pwd)/abcdefg" "$mask"
    $OST_DEBUG && {
        echo "running: get_line_number_matching $(pwd)/abcdefg $mask"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -ne 0 ]
    # classic / line exists
    binpath="$(pwd)/libexec/opensec-core"
    mask='/usr/bin/env'
    run get_line_number_matching "$binpath" "$mask"
    $OST_DEBUG && {
        echo "running: get_line_number_matching $binpath $mask"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ "$output" = "1" ]
    # classic / line not exists
    mask='/usr/bin/enva'
    run get_line_number_matching "$binpath" "$mask"
    $OST_DEBUG && {
        echo "running: get_line_number_matching $binpath $mask"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ -z "$output" ]
}

@test "[core-5]: str_to_upper" {
    # no arg error
    run str_to_upper
    $OST_DEBUG && {
        echo "running: str_to_upper"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -ne 0 ]
    # classic
    run str_to_upper "abcdefgHIJkLM"
    $OST_DEBUG && {
        echo "running: str_to_upper abcdefgHIJkLM"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ "$output" = 'ABCDEFGHIJKLM' ]
}

@test "[core-6]: str_to_lower" {
    # no arg error
    run str_to_lower
    $OST_DEBUG && {
        echo "running: str_to_lower"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -ne 0 ]
    # classic
    run str_to_lower "abcdefgHIJkLM"
    $OST_DEBUG && {
        echo "running: str_to_lower abcdefgHIJkLM"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ "$output" = 'abcdefghijklm' ]
}

@test "[core-7]: func_exists" {
    # classic / func exist
    run func_exists func_exists
    $OST_DEBUG && {
        echo "running: func_exists func_exists"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    # classic / func not exist
    run func_exists abcdefghijklmnopqrstuvw
    $OST_DEBUG && {
        echo "running: func_exists abcdefghijklmnopqrstuvw"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -ne 0 ]
}

@test "[core-8]: is_editable" {
    # no arg error
    run is_editable
    $OST_DEBUG && {
        echo "running: is_editable"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -ne 0 ]
    # path error
    run is_editable "$(pwd)/abcdefg"
    $OST_DEBUG && {
        echo "running: is_editable $(pwd)/abcdefg"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -ne 0 ]
    # classic / text file
    run is_editable "${OST_FIXTURES_LOREMIPSUMDIR}/${OST_CONTENTS_FILE}"
    $OST_DEBUG && {
        echo "running: is_editable ${OST_FIXTURES_LOREMIPSUMDIR}/${OST_CONTENTS_FILE}"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    # classic / image file
    run is_editable "${OST_FIXTURES_LOREMIPSUMDIR}/${OST_CONTENTS_IMAGE}"
    $OST_DEBUG && {
        echo "running: is_editable ${OST_FIXTURES_LOREMIPSUMDIR}/${OST_CONTENTS_IMAGE}"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -ne 0 ]
}

@test "[core-9]: is_tarball" {
    # no arg error
    run is_tarball
    $OST_DEBUG && {
        echo "running: is_tarball"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -ne 0 ]
    # path error
    run is_tarball "$(pwd)/abcdefg"
    $OST_DEBUG && {
        echo "running: is_tarball $(pwd)/abcdefg"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -ne 0 ]
    # classic / tarball
    run is_tarball "${OST_FIXTURES_PACKAGEDIR_TARBALL}"
    $OST_DEBUG && {
        echo "running: is_tarball ${OST_FIXTURES_PACKAGEDIR_TARBALL}"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    # classic / no tarball
    run is_tarball "${OST_FIXTURES_PACKAGEDIR_TARBALLSUM}"
    $OST_DEBUG && {
        echo "running: is_tarball ${OST_FIXTURES_PACKAGEDIR_TARBALLSUM}"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -ne 0 ]
}

@test "[core-10]: is_encrypted" {
    # no arg error
    run is_encrypted
    $OST_DEBUG && {
        echo "running: is_encrypted"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -ne 0 ]
    # path error
    run is_encrypted "$(pwd)/abcdefg"
    $OST_DEBUG && {
        echo "running: is_encrypted $(pwd)/abcdefg"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -ne 0 ]
    # classic / tarball
    run is_encrypted "${OST_FIXTURES_PACKAGEDIR_ENCRYPTED}"
    $OST_DEBUG && {
        echo "running: is_encrypted ${OST_FIXTURES_PACKAGEDIR_ENCRYPTED}"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    # classic / no tarball
    run is_encrypted "${OST_FIXTURES_PACKAGEDIR_TARBALLSUM}"
    $OST_DEBUG && {
        echo "running: is_encrypted ${OST_FIXTURES_PACKAGEDIR_TARBALLSUM}"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -ne 0 ]
}

@test "[core-11]: get_path_status" {
    # no arg error
    run get_path_status
    $OST_DEBUG && {
        echo "running: get_path_status"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -ne 0 ]
    # path error
    run get_path_status "$(pwd)/abcdefg"
    $OST_DEBUG && {
        echo "running: get_path_status $(pwd)/abcdefg"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -ne 0 ]
    # classic / encrypted
    run get_path_status "${OST_FIXTURES_PACKAGEDIR_ENCRYPTED}"
    $OST_DEBUG && {
        echo "running: get_path_status ${OST_FIXTURES_PACKAGEDIR_ENCRYPTED}"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [[ $output -eq $STATUS_TARBALL_ENCRYPTED ]]
    # classic / tarball
    run get_path_status "${OST_FIXTURES_PACKAGEDIR_TARBALL}"
    $OST_DEBUG && {
        echo "running: get_path_status ${OST_FIXTURES_PACKAGEDIR_TARBALL}"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [[ $output -eq $STATUS_TARBALL ]]
    # classic / opened
    run get_path_status "${OST_FIXTURES_PACKAGEDIR_OPENED}"
    $OST_DEBUG && {
        echo "running: get_path_status ${OST_FIXTURES_PACKAGEDIR_OPENED}"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [[ $output -eq $STATUS_OPENED ]]
    # classic / other
    run get_path_status "${OST_FIXTURES_LOREMIPSUMDIR}"
    $OST_DEBUG && {
        echo "running: get_path_status ${OST_FIXTURES_LOREMIPSUMDIR}"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [[ $output -eq $STATUS_OPENED ]]
}

@test "[core-12]: get_path_sum" {
    # no arg error
    run get_path_sum
    $OST_DEBUG && {
        echo "running: get_path_sum"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -ne 0 ]
    # path error
    run get_path_sum "$(pwd)/abcdefg"
    $OST_DEBUG && {
        echo "running: get_path_sum $(pwd)/abcdefg"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -ne 0 ]
    # classic
    run get_path_sum "${OST_FIXTURES_PACKAGEDIR_TARBALL}"
    $OST_DEBUG && {
        echo "running: get_path_status ${OST_FIXTURES_PACKAGEDIR_TARBALL}"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ "$output" = "$(cat "$OST_FIXTURES_PACKAGEDIR_TARBALLSUM")" ]
}

@test "[core-13]: get_password_sum" {
    # no arg error
    run get_password_sum
    $OST_DEBUG && {
        echo "running: get_password_sum"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -ne 0 ]
    # classic
    run get_password_sum "${OST_USER}"
    $OST_DEBUG && {
        echo "running: get_password_sum ${OST_USER}"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ "$output" = "$OST_USER_SHA1" ]
}
