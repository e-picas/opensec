#!/usr/bin/env bats
#
# <http://github.com/piwi/opensec>
# Copyright (c) 2015 Pierre Cassat & contributors
# License GNU-GPL v 3.0 - This program comes with ABSOLUTELY NO WARRANTY.
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code or see <http://www.gnu.org/licenses/>.
#
# Test for "appinfo" commands: help / usage / about / version
#

load test-commons

@test "[appinfo-1]: the version argument" {
    run "$OPENSECLIBEXEC/opensec" version
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec version"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ "${#lines[@]}" -eq 1 ]
    [[ "${lines[0]}" = *"$APPNAME $APPVERSION"* ]]
}

@test "[appinfo-2]: the about argument" {
    run "$OPENSECLIBEXEC/opensec" about
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec about"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ "${#lines[@]}" -gt 1 ]
    [ "${lines[0]}" = "$("$OPENSECLIBEXEC/opensec" version)" ]
}

@test "[appinfo-3]: the help global command" {
    run "$OPENSECLIBEXEC/opensec" help
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec help"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ "${#lines[@]}" -gt 1 ]
    [ "${lines[0]}" = "$("$OPENSECLIBEXEC/opensec" version)" ]
}

@test "[appinfo-4]: the usage argument" {
    run "$OPENSECLIBEXEC/opensec" usage
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec usage"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ "${#lines[@]}" -gt 1 ]
    helpoutputlines="$("$OPENSECLIBEXEC/opensec" help 2>&1 | wc -l)"
    [ "${#lines[@]}" -lt "$helpoutputlines" ]
}

@test "[appinfo-5]: the help command of a sub-command" {
    run "$OPENSECLIBEXEC/opensec" help status
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec help status"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ "${#lines[@]}" -gt 4 ]
}

@test "[appinfo-6]: the help command of all sub-commands" {
    for f in ${OPENSECLIBEXEC}/opensec-*; do
        cmd="${f/${OPENSECLIBEXEC}\/opensec-/}"
        if [ -n "$cmd" ] && [ "$cmd" != 'core' ] && [ "$cmd" != 'help' ]; then
            run "$OPENSECLIBEXEC/opensec" help "$cmd"
            $OST_DEBUG && {
                echo "running: $OPENSECLIBEXEC/opensec help $cmd"
                echo "output: $output"
                echo "status: $status"
                echo "lines: ${#lines[@]}"
            } >&1
            [ "$status" -eq 0 ]
            [ "${#lines[@]}" -gt 1 ]
        fi
    done
}
