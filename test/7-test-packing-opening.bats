#!/usr/bin/env bats
#
# <http://github.com/piwi/opensec>
# Copyright (c) 2015 Pierre Cassat & contributors
# License GNU-GPL v 3.0 - This program comes with ABSOLUTELY NO WARRANTY.
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code or see <http://www.gnu.org/licenses/>.
#
# Test for "packing" commands: compress / extract / encrypt / decrypt / open / close
#

load test-commons

setup(){
    tests_setup;
    make_closed_package_encrypted;
}

teardown(){
    tests_teardown;
}

@test "[packing-100]: decrypt of encrypted package" {
    package="${OST_TESTDIR}/$(basename "$OST_FIXTURES_PACKAGEDIR_ENCRYPTED")"
    run "$OPENSECLIBEXEC/opensec" decrypt "$package" "$OST_USER"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec decrypt $package $OST_USER"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ -f "${package/${EXT_CRYPTED}/}" ]
}

@test "[packing-101]: decrypt of encrypted package and no FORCED mode" {
    skip 'MANUAL'
}

@test "[packing-102]: decrypt a non-existing package (status>0)" {
    run "$OPENSECLIBEXEC/opensec" decrypt "$OST_TMP_PACKAGEabcdefghijklmnopqrstuvw"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec decrypt $OST_TMP_PACKAGEabcdefghijklmnopqrstuvw"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -ne 0 ]
}

@test "[packing-103]: decrypt an already decrypted package (status=0)" {
    make_closed_package_compressed;
    package="${OST_TESTDIR}/$(basename "$OST_FIXTURES_PACKAGEDIR_TARBALL")"
    run "$OPENSECLIBEXEC/opensec" decrypt "$package"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec decrypt $package"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
}

@test "[packing-104]: uncompress of compressed package" {
    # decrypt package
    package="${OST_TESTDIR}/$(basename "$OST_FIXTURES_PACKAGEDIR_ENCRYPTED")"
    run "$OPENSECLIBEXEC/opensec" decrypt "$package" "$OST_USER"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec decrypt $package $OST_USER"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ -f "${package/${EXT_CRYPTED}/}" ]
    # uncompress it
    package="${package/${EXT_CRYPTED}/}"
    [ -f "${package}${EXT_SUM}" ]
    run "$OPENSECLIBEXEC/opensec" extract "$package"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec extract $package"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ -d "${package/${EXT_TARBALL}/}" ]
    [ -f "${package/${EXT_TARBALL}/}/${DIR_DATA}${EXT_TARBALL}${EXT_CRYPTED}" ]
    [ -f "${package/${EXT_TARBALL}/}/${DIR_DATA}${EXT_TARBALL}${EXT_SUM}" ]
}

@test "[packing-105]: uncompress of compressed package and no FORCED mode" {
    skip 'MANUAL'
}

@test "[packing-106]: uncompress a non-existing package (status>0)" {
    run "$OPENSECLIBEXEC/opensec" decrypt "$OST_TMP_PACKAGEabcdefghijklmnopqrstuvw"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec decrypt $OST_TMP_PACKAGEabcdefghijklmnopqrstuvw"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -ne 0 ]
}

@test "[packing-107]: uncompress an already uncompressed package (status=0)" {
    make_opened_package_fullyopened;
    package="${OST_TESTDIR}/$(basename "$OST_FIXTURES_PACKAGEDIR_OPENED")"
    run "$OPENSECLIBEXEC/opensec" decrypt "$package"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec decrypt $package"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
}

@test "[packing-108]: open of compressed and encrypted package" {
    package="${OST_TESTDIR}/$(basename "$OST_FIXTURES_PACKAGEDIR_ENCRYPTED")"
    run "$OPENSECLIBEXEC/opensec" open "$package" "$OST_USER"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec open $package $OST_USER"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    package="${package/${EXT_CRYPTED}/}"
    [ -d "${package/${EXT_TARBALL}/}" ]
    [ -f "${package/${EXT_TARBALL}/}/${DIR_DATA}${EXT_TARBALL}${EXT_CRYPTED}" ]
    [ -f "${package/${EXT_TARBALL}/}/${DIR_DATA}${EXT_TARBALL}${EXT_SUM}" ]
}

@test "[packing-109]: open of compressed and encrypted package and no FORCED mode" {
    skip 'MANUAL'
}

@test "[packing-110]: decrypt of encrypted and compressed package's data" {
    # open package
    package="${OST_TESTDIR}/$(basename "$OST_FIXTURES_PACKAGEDIR_ENCRYPTED")"
    run "$OPENSECLIBEXEC/opensec" open "$package" "$OST_USER"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec open $package $OST_USER"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    package="${package/${EXT_CRYPTED}/}"
    package="${package/${EXT_TARBALL}/}"
    [ -d "${package}" ]
    [ -f "${package}/${DIR_DATA}${EXT_TARBALL}${EXT_CRYPTED}" ]
    [ -f "${package}/${DIR_DATA}${EXT_TARBALL}${EXT_SUM}" ]
    # decrypt data
    run "$OPENSECLIBEXEC/opensec" decrypt-data "$package" "$OST_PASS"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec decrypt-data $package $OST_PASS"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ -f "${package}/${DIR_DATA}${EXT_TARBALL}" ]
}

@test "[packing-111]: decrypt of encrypted and compressed package's data and no FORCED mode" {
    skip 'MANUAL'
}

@test "[packing-112]: decrypt data of a non-existing package (status>0)" {
    run "$OPENSECLIBEXEC/opensec" decrypt-data "$OST_TMP_PACKAGEabcdefghijklmnopqrstuvw"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec decrypt-data $OST_TMP_PACKAGEabcdefghijklmnopqrstuvw"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -ne 0 ]
}

@test "[packing-113]: decrypt already decrypted package's data (status=0)" {
    make_opened_package_data_compressed;
    package="${OST_TESTDIR}/$(basename "$OST_FIXTURES_PACKAGEDIR_DATATARBALL")"
    run "$OPENSECLIBEXEC/opensec" decrypt-data "$package" "$OST_PASS"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec decrypt-data $package $OST_PASS"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
}

@test "[packing-114]: extact of compressed package's data" {
    # open package
    package="${OST_TESTDIR}/$(basename "$OST_FIXTURES_PACKAGEDIR_ENCRYPTED")"
    run "$OPENSECLIBEXEC/opensec" open "$package" "$OST_USER"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec open $package $OST_USER"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    package="${package/${EXT_CRYPTED}/}"
    package="${package/${EXT_TARBALL}/}"
    [ -d "${package}" ]
    [ -f "${package}/${DIR_DATA}${EXT_TARBALL}${EXT_CRYPTED}" ]
    [ -f "${package}/${DIR_DATA}${EXT_TARBALL}${EXT_SUM}" ]
    # decrypt data
    run "$OPENSECLIBEXEC/opensec" decrypt-data "$package" "$OST_PASS"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec decrypt-data $package $OST_PASS"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ -f "${package}/${DIR_DATA}${EXT_TARBALL}" ]
    # uncompress it
    run "$OPENSECLIBEXEC/opensec" extract-data "$package"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec extract-data $package"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ -d "${package}/${DIR_DATA}" ]
}

@test "[packing-115]: extact of compressed package's data and no FORCED mode" {
    skip 'MANUAL'
}

@test "[packing-116]: extact data of a non-existing package (status>0)" {
    run "$OPENSECLIBEXEC/opensec" decrypt-data "$OST_TMP_PACKAGEabcdefghijklmnopqrstuvw"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec decrypt-data $OST_TMP_PACKAGEabcdefghijklmnopqrstuvw"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -ne 0 ]
}

@test "[packing-117]: extact already uncompressed package's data (status=0)" {
    make_opened_package_fullyopened;
    package="${OST_TESTDIR}/$(basename "$OST_FIXTURES_PACKAGEDIR_OPENED")"
    run "$OPENSECLIBEXEC/opensec" decrypt-data "$package"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec decrypt-data $package"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
}

@test "[packing-118]: open of compressed and encrypted package's data" {
    # open package
    package="${OST_TESTDIR}/$(basename "$OST_FIXTURES_PACKAGEDIR_ENCRYPTED")"
    run "$OPENSECLIBEXEC/opensec" open "$package" "$OST_USER"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec open $package $OST_USER"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    package="${package/${EXT_CRYPTED}/}"
    package="${package/${EXT_TARBALL}/}"
    [ -d "${package}" ]
    [ -f "${package}/${DIR_DATA}${EXT_TARBALL}${EXT_CRYPTED}" ]
    [ -f "${package}/${DIR_DATA}${EXT_TARBALL}${EXT_SUM}" ]
    # open data
    run "$OPENSECLIBEXEC/opensec" open-data "$package" "$OST_PASS"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec open-data $package $OST_PASS"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ -d "${package}/${DIR_DATA}" ]
}

@test "[packing-119]: open of compressed and encrypted package's data and no FORCED mode" {
    skip 'MANUAL'
}
