#!/usr/bin/env bats
#
# <http://github.com/piwi/opensec>
# Copyright (c) 2015 Pierre Cassat & contributors
# License GNU-GPL v 3.0 - This program comes with ABSOLUTELY NO WARRANTY.
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code or see <http://www.gnu.org/licenses/>.
#
# Test for "init" commands: create / init
#

load test-commons

setup() { tests_setup; }

teardown() { tests_teardown; }

@test "[init-1]: simple creation test" {
    teardown
    run "$OPENSECLIBEXEC/opensec" create "$OST_TMP_PACKAGE"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec create $OST_TMP_PACKAGE"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ -e "$OST_TMP_PACKAGE" ] && [ -d "$OST_TMP_PACKAGE" ]
    descfile="$(get_package_description_path "$OST_TMP_PACKAGE")"
    [ -e "$descfile" ] && [ -f "$descfile" ]
    logfile="$(get_package_log_path "$OST_TMP_PACKAGE")"
    [ -e "$logfile" ] && [ -f "$logfile" ]
    cluefile="$(get_package_clue_path "$OST_TMP_PACKAGE")"
    [ -e "$cluefile" ] && [ -f "$cluefile" ]
    datapath="$(get_package_data_path "$OST_TMP_PACKAGE")"
    [ -e "$datapath" ] && [ -d "$datapath" ]
}

@test "[init-2]: test of creation on existing path (status>0)" {
    run "$OPENSECLIBEXEC/opensec" create "$OST_TMP_PACKAGE"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec create $OST_TMP_PACKAGE"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -ne 0 ]
}

@test "[init-3]: creation test with a description" {
    teardown
    run "$OPENSECLIBEXEC/opensec" create "$OST_TMP_PACKAGE" "$OST_DESC"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec create $OST_TMP_PACKAGE $OST_DESC"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ -e "$OST_TMP_PACKAGE" ] && [ -d "$OST_TMP_PACKAGE" ]
    descfile="$(get_package_description_path "$OST_TMP_PACKAGE")"
    [ -f "$descfile" ]
    desccontent="$(cat "$descfile")"
    [ "$desccontent" = "$OST_DESC" ]
}

@test "[init-4]: simple init in an empty directory" {
    run "$OPENSECLIBEXEC/opensec" init "$OST_TMP_PACKAGE"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec init $OST_TMP_PACKAGE"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    descfile="$(get_package_description_path "$OST_TMP_PACKAGE")"
    [ -e "$descfile" ] && [ -f "$descfile" ]
    logfile="$(get_package_log_path "$OST_TMP_PACKAGE")"
    [ -e "$logfile" ] && [ -f "$logfile" ]
    cluefile="$(get_package_clue_path "$OST_TMP_PACKAGE")"
    [ -e "$cluefile" ] && [ -f "$cluefile" ]
    datapath="$(get_package_data_path "$OST_TMP_PACKAGE")"
    [ -e "$datapath" ] && [ -d "$datapath" ]
}

@test "[init-5]: test of init on a non-existing path (status>0)" {
    teardown
    run "$OPENSECLIBEXEC/opensec" init "$OST_TMP_PACKAGE"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec init $OST_TMP_PACKAGE"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -ne 0 ]
}

@test "[init-6]: test of init in a directory with contents and FORCED mode" {
    cp -r "$OST_FIXTURES_LOREMIPSUMDIR"/* "$OST_TMP_PACKAGE"
    run "$OPENSECLIBEXEC/opensec" -f init "$OST_TMP_PACKAGE"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec -f init $OST_TMP_PACKAGE"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    datapath="$(get_package_data_path "$OST_TMP_PACKAGE")"
    for f in $OST_FIXTURES_LOREMIPSUMDIR/*; do
        fname="${f/$OST_FIXTURES_LOREMIPSUMDIR\//}"
        [ -e "${datapath}/${fname}" ]
    done
}

@test "[init-7]: test of init in a directory with contents and no FORCED mode" {
    cp -r "$OST_FIXTURES_LOREMIPSUMDIR"/* "$OST_TMP_PACKAGE"
    run "$OPENSECLIBEXEC/opensec" init "$OST_TMP_PACKAGE" <<< 'y'
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec init $OST_TMP_PACKAGE"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    datapath="$(get_package_data_path "$OST_TMP_PACKAGE")"
    for f in $OST_FIXTURES_LOREMIPSUMDIR/*; do
        fname="${f/$OST_FIXTURES_LOREMIPSUMDIR\//}"
        [ -e "${datapath}/${fname}" ]
    done
}

@test "[init-8]: test of init with a description" {
    run "$OPENSECLIBEXEC/opensec" init "$OST_TMP_PACKAGE" "$OST_DESC"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec init $OST_TMP_PACKAGE $OST_DESC"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ -e "$OST_TMP_PACKAGE" ] && [ -d "$OST_TMP_PACKAGE" ]
    descfile="$(get_package_description_path "$OST_TMP_PACKAGE")"
    [ -f "$descfile" ]
    desccontent="$(cat "$descfile")"
    [ "$desccontent" = "$OST_DESC" ]
}
