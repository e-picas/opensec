#!/usr/bin/env bats
#
# <http://github.com/piwi/opensec>
# Copyright (c) 2015 Pierre Cassat & contributors
# License GNU-GPL v 3.0 - This program comes with ABSOLUTELY NO WARRANTY.
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code or see <http://www.gnu.org/licenses/>.
#
# Test for the "config" command
#

load test-commons

# prepare empty package with configuration
tests_setup;
make_opened_package_fullyopened
package="${OST_TESTDIR}/$(basename "${OST_FIXTURES_PACKAGEDIR_OPENED}")"
cp "$OST_FIXTURES_CONFIG" "${package}/"

# backup user config if so
ROLLBACK=false
USERCFG="${HOME}/.opensec"

setup() {
    [ -f "${USERCFG}" ] && mv "$USERCFG" "${USERCFG}.bak" || return 0;
}

teardown() {
    rm -f "$USERCFG";
    [ -f "${USERCFG}.bak" ] && mv "${USERCFG}.bak" "$USERCFG" || return 0;
}

@test "[config-1]: read global config" {
    # -g option
    run "$OPENSECLIBEXEC/opensec" -g config
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec -g config"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    # single var
    run "$OPENSECLIBEXEC/opensec" -g config dir.data
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec -g config dir.data"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ "$output" = "data" ]
}

@test "[config-2]: set global config value" {
    # no user config
    [ ! -f "$USERCFG" ]
    # single var
    run "$OPENSECLIBEXEC/opensec" -g config dir.data
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec -g config dir.data"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ "$output" = "data" ]
    # set value
    run "$OPENSECLIBEXEC/opensec" -g config dir.data 'data-test'
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec -g config dir.data 'data-test'"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    # user config
    [ -f "$USERCFG" ]
    # single var
    run "$OPENSECLIBEXEC/opensec" -g config dir.data
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec -g config dir.data"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ "$output" = "data-test" ]
}

@test "[config-3]: read local value" {
    # single var
    run "$OPENSECLIBEXEC/opensec" config "$package" dir.data
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec config $package dir.data"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ "$output" = "data-test" ]
}

@test "[config-4]: set local config value" {
    # single var
    run "$OPENSECLIBEXEC/opensec" config "$package" dir.data
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec config $package dir.data"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ "$output" = "data-test" ]
    # set value
    run "$OPENSECLIBEXEC/opensec" config "$package" dir.data new-data-test
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec config $package dir.data new-data-test"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    # single var
    run "$OPENSECLIBEXEC/opensec" config "$package" dir.data
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec config $package dir.data"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ "$output" = "new-data-test" ]
}
