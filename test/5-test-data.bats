#!/usr/bin/env bats
#
# <http://github.com/piwi/opensec>
# Copyright (c) 2015 Pierre Cassat & contributors
# License GNU-GPL v 3.0 - This program comes with ABSOLUTELY NO WARRANTY.
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code or see <http://www.gnu.org/licenses/>.
#
# Test for "data" commands: add / edit / remove / replace / show / list
#

load test-commons

setup(){ tests_setup; }

teardown(){ tests_teardown; }

@test "[data-1]: list all contents of a package's data" {
    make_opened_package
    run "$OPENSECLIBEXEC/opensec" list "$OST_TMP_PACKAGE"
    loremipsumdata="$(get_package_data_path "$OST_FIXTURES_PACKAGEDIR_OPENED")"
    testpackagedata="$(get_package_data_path "$OST_TMP_PACKAGE")"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec list $OST_TMP_PACKAGE"
        echo "output: $output"
        echo "status: $status"
        echo "sould be: $(ls -ARlh "$loremipsumdata")"
    } >&1
    [ "$status" -eq 0 ]
    sampleoutput="$(ls -ARlh "$loremipsumdata")"
    [ "$(tail -n +2 "$output")" = "$(tail -n +2 "$sampleoutput")" ]
}

@test "[data-2]: show content of a text file" {
    make_opened_package
    run "$OPENSECLIBEXEC/opensec" show "$OST_TMP_PACKAGE" "$OST_CONTENTS_FILE"
    loremipsumtestfile="$(get_package_data_path "$OST_FIXTURES_PACKAGEDIR_OPENED" "$OST_CONTENTS_FILE")"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec show $OST_TMP_PACKAGE $OST_CONTENTS_FILE"
        echo "output: $output"
        echo "status: $status"
        echo "sould be: $(cat "$loremipsumtestfile")"
    } >&1
    samplectt="$(cat "$loremipsumtestfile")"
    [ "$status" -eq 0 ]
    [[ "$(strip_empty_lines "$output")" = *"$(strip_empty_lines "$samplectt")"* ]]
}

@test "[data-3]: show content of an image file" {
    skip 'MANUAL'
}

@test "[data-4]: show contents of mutiple files in a single command" {
    make_opened_package
    run "$OPENSECLIBEXEC/opensec" show "$OST_TMP_PACKAGE" "$OST_CONTENTS_FILE" "$OST_CONTENTS_FILEBIS"
    loremipsumtestfile="$(get_package_data_path "$OST_FIXTURES_PACKAGEDIR_OPENED" "$OST_CONTENTS_FILE")"
    loremipsumtestfilebis="$(get_package_data_path "$OST_FIXTURES_PACKAGEDIR_OPENED" "$OST_CONTENTS_FILEBIS")"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec show $OST_TMP_PACKAGE $OST_CONTENTS_FILE $OST_CONTENTS_FILEBIS"
        echo "output: $output"
        echo "status: $status"
        echo "sould be: $(cat "$loremipsumtestfile")"
    } >&1
    samplectt="$(cat "$loremipsumtestfile")"
    samplecttbis="$(cat "$loremipsumtestfilebis")"
    [ "$status" -eq 0 ]
    [[ "$(strip_empty_lines "$output")" = *"$(strip_empty_lines "$samplectt")"* ]]
    [[ "$(strip_empty_lines "$output")" = *"$(strip_empty_lines "$samplecttbis")"* ]]
}

@test "[data-5]: edit content of a text file" {
    skip 'MANUAL'
}

@test "[data-6]: edit content of an image file" {
    skip 'MANUAL'
}

@test "[data-7]: add a simple file" {
    make_empty_package
    original="${OST_FIXTURES_LOREMIPSUMDIR}/${OST_CONTENTS_FILE}"
    target="$(get_package_data_path "${OST_TMP_PACKAGE}" "${OST_CONTENTS_FILE}")"
    [ -f "$original" ]
    [ ! -f "$target" ]
    run "$OPENSECLIBEXEC/opensec" add "$OST_TMP_PACKAGE" "$original"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec add $OST_TMP_PACKAGE $original"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ -f "$target" ]
}

@test "[data-8]: add a simple file setting a target file name" {
    make_empty_package
    original="${OST_FIXTURES_LOREMIPSUMDIR}/${OST_CONTENTS_FILE}"
    [ -f "$original" ]
    target="$(get_package_data_path "${OST_TMP_PACKAGE}" "${OST_CONTENTS_FILEBIS}")"
    [ ! -f "$target" ]
    cp "$original" "${OST_TMP_PACKAGE}/${DIR_DATA}"
    targetcompare="$(get_package_data_path "${OST_TMP_PACKAGE}" "${OST_CONTENTS_FILE}")"
    [ -f "$targetcompare" ]
    run "$OPENSECLIBEXEC/opensec" add "$OST_TMP_PACKAGE" "$original" "$OST_CONTENTS_FILEBIS"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec add $OST_TMP_PACKAGE $original $OST_CONTENTS_FILEBIS"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ -f "$target" ]
    [ "$(cat "$target")" = "$(cat "$targetcompare")" ]
}

@test "[data-9]: add a simple file setting an existing directory" {
    make_empty_package
    original="${OST_FIXTURES_LOREMIPSUMDIR}/${OST_CONTENTS_FILE}"
    [ -f "$original" ]
    target="$(get_package_data_path "${OST_TMP_PACKAGE}" "${OST_CONTENTS_SUBDIR}")"
    [ ! -e "$target" ]
    mkdir "$target"
    [ -d "$target" ]
    targetcompare="$(get_package_data_path "${OST_TMP_PACKAGE}" "${OST_CONTENTS_SUBDIR}/${OST_CONTENTS_FILE}")"
    [ ! -f "$targetcompare" ]
    run "$OPENSECLIBEXEC/opensec" add "$OST_TMP_PACKAGE" "$original" "$OST_CONTENTS_SUBDIR"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec add $OST_TMP_PACKAGE $original $OST_CONTENTS_SUBDIR"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ -f "$targetcompare" ]
}

@test "[data-10]: add a non-existing file (status>0)" {
    make_empty_package
    original="${OST_FIXTURES_LOREMIPSUMDIR}/abcdefghijklmnopqrstuvw"
    [ ! -f "$original" ]
    run "$OPENSECLIBEXEC/opensec" add "$OST_TMP_PACKAGE" "$original"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec add $OST_TMP_PACKAGE $original"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -ne 0 ]
}

@test "[data-11]: add a simple file setting an existing target file name (status=0)" {
    skip 'it fails randomly'
    make_empty_package
    original="${OST_FIXTURES_LOREMIPSUMDIR}/${OST_CONTENTS_FILE}"
    [ -f "$original" ]
    cp "$original" "${OST_TMP_PACKAGE}/${DIR_DATA}"
    target="$(get_package_data_path "${OST_TMP_PACKAGE}" "${OST_CONTENTS_FILE}")"
    [ -f "$target" ]
    begin="$(get_last_modification_time "$target")"
    run "$OPENSECLIBEXEC/opensec" add "$OST_TMP_PACKAGE" "$original"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec add $OST_TMP_PACKAGE $original"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    end="$(get_last_modification_time "$target")"
    [ "$end" = "$begin" ]
}

@test "[data-12]: add directory recursively" {
    make_empty_package
    original="${OST_FIXTURES_LOREMIPSUMDIR}/${OST_CONTENTS_SUBDIR}"
    target="$(get_package_data_path "${OST_TMP_PACKAGE}" "${OST_CONTENTS_SUBDIR}")"
    [ -d "$original" ]
    [ ! -e "$target" ]
    run "$OPENSECLIBEXEC/opensec" add "$OST_TMP_PACKAGE" "$original"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec add $OST_TMP_PACKAGE $original"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ -d "$target" ]
    testfile="$(get_package_data_path "${OST_TMP_PACKAGE}" "${OST_CONTENTS_SUBDIR}/${OST_CONTENTS_FILE}")"
    [ -f "$testfile" ]
}

@test "[data-13]: remove a simple file" {
    make_empty_package
    original="${OST_FIXTURES_LOREMIPSUMDIR}/${OST_CONTENTS_FILE}"
    [ -f "$original" ]
    cp "$original" "${OST_TMP_PACKAGE}/${DIR_DATA}"
    target="$(get_package_data_path "${OST_TMP_PACKAGE}" "${OST_CONTENTS_FILE}")"
    [ -f "$target" ]
    run "$OPENSECLIBEXEC/opensec" remove "$OST_TMP_PACKAGE" "$OST_CONTENTS_FILE"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec remove $OST_TMP_PACKAGE $OST_CONTENTS_FILE"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ ! -f "$target" ]
}

@test "[data-14]: remove directory recursively" {
    make_empty_package
    original="${OST_FIXTURES_LOREMIPSUMDIR}/${OST_CONTENTS_SUBDIR}"
    [ -d "$original" ]
    cp -r "$original" "${OST_TMP_PACKAGE}/${DIR_DATA}"
    target="$(get_package_data_path "${OST_TMP_PACKAGE}" "${OST_CONTENTS_SUBDIR}")"
    [ -d "$target" ]
    testfile="$(get_package_data_path "${OST_TMP_PACKAGE}" "${OST_CONTENTS_SUBDIR}/${OST_CONTENTS_FILE}")"
    [ -f "$testfile" ]
    run "$OPENSECLIBEXEC/opensec" remove "$OST_TMP_PACKAGE" "$OST_CONTENTS_SUBDIR"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec remove $OST_TMP_PACKAGE $OST_CONTENTS_SUBDIR"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ ! -d "$target" ]
}

@test "[data-15]: remove mutiple files in a single command" {
    make_empty_package

    originalfile="${OST_FIXTURES_LOREMIPSUMDIR}/${OST_CONTENTS_FILE}"
    [ -f "$originalfile" ]
    cp "$originalfile" "${OST_TMP_PACKAGE}/${DIR_DATA}"
    targetfile="$(get_package_data_path "${OST_TMP_PACKAGE}" "${OST_CONTENTS_FILE}")"
    [ -f "$targetfile" ]

    originaldir="${OST_FIXTURES_LOREMIPSUMDIR}/${OST_CONTENTS_SUBDIR}"
    [ -d "$originaldir" ]
    cp -r "$originaldir" "${OST_TMP_PACKAGE}/${DIR_DATA}"
    targetdir="$(get_package_data_path "${OST_TMP_PACKAGE}" "${OST_CONTENTS_SUBDIR}")"
    [ -d "$targetdir" ]
    testfile="$(get_package_data_path "${OST_TMP_PACKAGE}" "${OST_CONTENTS_SUBDIR}/${OST_CONTENTS_FILE}")"
    [ -f "$testfile" ]

    run "$OPENSECLIBEXEC/opensec" remove "$OST_TMP_PACKAGE" "$OST_CONTENTS_FILE" "$OST_CONTENTS_SUBDIR"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec remove $OST_TMP_PACKAGE $OST_CONTENTS_FILE $OST_CONTENTS_SUBDIR"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ ! -f "$targetfile" ]
    [ ! -d "$targetdir" ]
}

@test "[data-16]: remove a non-existing file (status>0)" {
    make_empty_package
    original="$(get_package_data_path "${OST_TMP_PACKAGE}" "abcdefghijklmnopqrstuvw")"
    [ ! -f "$original" ]
    run "$OPENSECLIBEXEC/opensec" remove "$OST_TMP_PACKAGE" abcdefghijklmnopqrstuvw
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec remove $OST_TMP_PACKAGE abcdefghijklmnopqrstuvw"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -ne 0 ]
}

@test "[data-17]: show content of a text file using a full package's path (with package name)" {
    make_opened_package
    path="$(basename "$OST_TMP_PACKAGE")/${DIR_DATA}/${OST_CONTENTS_FILE}"
    run "$OPENSECLIBEXEC/opensec" show "$OST_TMP_PACKAGE" "$path"
    loremipsumtestfile="$(get_package_data_path "$OST_FIXTURES_PACKAGEDIR_OPENED" "$OST_CONTENTS_FILE")"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec show $OST_TMP_PACKAGE $path"
        echo "output: $output"
        echo "status: $status"
        echo "sould be: $(cat "$loremipsumtestfile")"
    } >&1
    samplectt="$(cat "$loremipsumtestfile")"
    [ "$status" -eq 0 ]
    [[ "$(strip_empty_lines "$output")" = *"$(strip_empty_lines "$samplectt")"* ]]
}
