#!/usr/bin/env bats
#
# <http://github.com/piwi/opensec>
# Copyright (c) 2015 Pierre Cassat & contributors
# License GNU-GPL v 3.0 - This program comes with ABSOLUTELY NO WARRANTY.
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code or see <http://www.gnu.org/licenses/>.
#
# Test for "meta" commands: status / log / clue / description / check
#

load test-commons

setup(){ tests_setup; }

teardown(){ tests_teardown; }

@test "[meta-1]: status of a package" {
    # opened package
    make_opened_package
    run "$OPENSECLIBEXEC/opensec" status "$OST_TMP_PACKAGE"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec status $OST_TMP_PACKAGE"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    # compressed data package
    make_opened_package_data_compressed
    run "$OPENSECLIBEXEC/opensec" status "$OST_TMP_PACKAGE"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec status $OST_TMP_PACKAGE"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    # encrypted data package
    make_opened_package_data_encrypted
    run "$OPENSECLIBEXEC/opensec" status "$OST_TMP_PACKAGE"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec status $OST_TMP_PACKAGE"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    # compressed package
    make_closed_package_compressed
    run "$OPENSECLIBEXEC/opensec" status "$OST_TMP_PACKAGE"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec status $OST_TMP_PACKAGE"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    # encrypted package
    make_closed_package_encrypted
    run "$OPENSECLIBEXEC/opensec" status "$OST_TMP_PACKAGE"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec status $OST_TMP_PACKAGE"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
}

@test "[meta-2]: status of a none package (status>0)" {
    # empty directory not a package
    make_no_package
    run "$OPENSECLIBEXEC/opensec" status "$OST_TMP_PACKAGE"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec status $OST_TMP_PACKAGE"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -ne 0 ]
    # non-existant directory
    teardown
    run "$OPENSECLIBEXEC/opensec" status "$OST_TMP_PACKAGE"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec status $OST_TMP_PACKAGE"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -ne 0 ]
}

@test "[meta-3]: new log in a package" {
    make_opened_package
    logfile="$(get_package_log_path "$OST_TMP_PACKAGE")"
    [ -f "$logfile" ]
    originalln="$(get_number_of_lines "$logfile")"
    run "$OPENSECLIBEXEC/opensec" log "$OST_TMP_PACKAGE" 'my log message'
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec log $OST_TMP_PACKAGE 'my log message'"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    newln="$(get_number_of_lines "$logfile")"
    [ "$newln" -gt "$originalln" ]
}

@test "[meta-4]: read logs of a package" {
    make_opened_package
    logfile="$(get_package_log_path "$OST_TMP_PACKAGE")"
    [ -f "$logfile" ]
    echo "$OST_DESC" >> "$logfile"
    run "$OPENSECLIBEXEC/opensec" log "$OST_TMP_PACKAGE"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec log $OST_TMP_PACKAGE"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ "${#lines[*]}" -gt 0 ]
}

@test "[meta-5]: read description of a package" {
    make_opened_package
    descfile="$(get_package_description_path "$OST_TMP_PACKAGE")"
    [ -f "$descfile" ]
    run "$OPENSECLIBEXEC/opensec" description "$OST_TMP_PACKAGE"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec description $OST_TMP_PACKAGE"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ -n "$output" ]
    [ "$(strip_empty_lines "$output")" = "$(strip_empty_lines "$(cat "$descfile")")" ]
}

@test "[meta-6]: set description of a package" {
    make_opened_package
    descfile="$(get_package_description_path "$OST_TMP_PACKAGE")"
    [ -f "$descfile" ]
    [ "$(strip_empty_lines "$(cat "$descfile")")" = "$OST_DESC" ]
    run "$OPENSECLIBEXEC/opensec" description "$OST_TMP_PACKAGE" "${OST_DESC} ${OST_LOREM}"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec description $OST_TMP_PACKAGE '${OST_DESC} ${OST_LOREM}'"
        echo "output: $output"
        echo "status: $status"
        echo "should be: ${OST_DESC} ${OST_LOREM}"
    } >&1
    [ "$status" -eq 0 ]
    [ "$(strip_empty_lines "$(cat "$descfile")")" = "${OST_DESC} ${OST_LOREM}" ]
}

@test "[meta-7]: test of description edit" {
    skip 'MANUAL'
}

@test "[meta-8]: read clue of a package" {
    make_opened_package
    cluefile="$(get_package_clue_path "$OST_TMP_PACKAGE")"
    [ -f "$cluefile" ]
    run "$OPENSECLIBEXEC/opensec" clue "$OST_TMP_PACKAGE"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec clue $OST_TMP_PACKAGE"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ -n "$output" ]
    [ "$(strip_empty_lines "$output")" = "$(strip_empty_lines "$(cat "$cluefile")")" ]
}

@test "[meta-9]: set clue of a package" {
    make_opened_package
    cluefile="$(get_package_clue_path "$OST_TMP_PACKAGE")"
    [ -f "$cluefile" ]
    [ "$(strip_empty_lines "$(cat "$cluefile")")" = "$OST_CLUE" ]
    run "$OPENSECLIBEXEC/opensec" clue "$OST_TMP_PACKAGE" "${OST_CLUE} ${OST_LOREM}"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec clue $OST_TMP_PACKAGE '${OST_CLUE} ${OST_LOREM}'"
        echo "output: $output"
        echo "status: $status"
        echo "should be: ${OST_CLUE} ${OST_LOREM}"
    } >&1
    [ "$status" -eq 0 ]
    [ "$(strip_empty_lines "$(cat "$cluefile")")" = "${OST_CLUE} ${OST_LOREM}" ]
}

@test "[meta-10]: test of clue edit" {
    skip 'MANUAL'
}

@test "[meta-11]: test of clue prompt" {
    skip 'MANUAL'
}

@test "[meta-12]: check package integrity (ok)" {
    make_closed_package_compressed
    package="${OST_TESTDIR}/$(basename "$OST_FIXTURES_PACKAGEDIR_TARBALL")"
    [ -f "$package" ]
    [ -f "${package}${EXT_SUM}" ]
    run "$OPENSECLIBEXEC/opensec" check "$package"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec check $package"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ -n "$output" ]
}

@test "[meta-13]: check package integrity (ko - status=0)" {
    make_closed_package_compressed
    package="${OST_TESTDIR}/$(basename "$OST_FIXTURES_PACKAGEDIR_TARBALL")"
    [ -f "$package" ]
    [ -f "${package}${EXT_SUM}" ]
    echo '1234' >> "${package}${EXT_SUM}"
    run "$OPENSECLIBEXEC/opensec" check "$package"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec check $package"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ -n "$output" ]
}

@test "[meta-14]: check package integrity in VERBOSE mode (ko - status>0)" {
    make_closed_package_compressed
    package="${OST_TESTDIR}/$(basename "$OST_FIXTURES_PACKAGEDIR_TARBALL")"
    [ -f "$package" ]
    [ -f "${package}${EXT_SUM}" ]
    echo '1234' >> "${package}${EXT_SUM}"
    run "$OPENSECLIBEXEC/opensec" -v check "$package"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec -v check $package"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -ne 0 ]
}

@test "[meta-15]: check package's data integrity (ok)" {
    make_closed_package_compressed
    package="${OST_TESTDIR}/$(basename "$OST_FIXTURES_PACKAGEDIR_TARBALL")"
    [ -f "$package" ]
    [ -f "${package}${EXT_SUM}" ]
    run "$OPENSECLIBEXEC/opensec" check-data "$package"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec check-data $package"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ -n "$output" ]
}

@test "[meta-16]: check package's data integrity (ko - status=0)" {
    make_opened_package_data_compressed
    package="${OST_TESTDIR}/$(basename "$OST_FIXTURES_PACKAGEDIR_DATATARBALL")"
    [ -d "$package" ]
    [ -f "${package}/${DIR_DATA}${EXT_TARBALL}${EXT_SUM}" ]
    echo '1234' >> "${package}/${DIR_DATA}${EXT_TARBALL}${EXT_SUM}"
    run "$OPENSECLIBEXEC/opensec" check-data "$package"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec check-data $package"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ -n "$output" ]
}

@test "[meta-17]: check package's data integrity in VERBOSE mode (ko - status>0)" {
    make_opened_package_data_compressed
    package="${OST_TESTDIR}/$(basename "$OST_FIXTURES_PACKAGEDIR_DATATARBALL")"
    [ -d "$package" ]
    [ -f "${package}/${DIR_DATA}${EXT_TARBALL}${EXT_SUM}" ]
    echo '1234' >> "${package}/${DIR_DATA}${EXT_TARBALL}${EXT_SUM}"
    run "$OPENSECLIBEXEC/opensec" -v check-data "$package"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec -v check-data $package"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -ne 0 ]
}

@test "[meta-18]: check package's data integrity automatically (ok)" {
    make_closed_package_compressed
    package="${OST_TESTDIR}/$(basename "$OST_FIXTURES_PACKAGEDIR_TARBALL")"
    [ -f "$package" ]
    [ -f "${package}${EXT_SUM}" ]
    run "$OPENSECLIBEXEC/opensec" check "$package"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec check $package"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ -n "$output" ]
    [[ "$output" = *"seems good"* ]]
}
