The tests are run with the help of <http://github.com/sstephenson/bats>.

To run the tests, use:

    bats test/*.bats
    # or
    ./test/run-all.sh

Some special tests with a manual action can be run with:

    bats test/*.bats.manual

Any test file must be named `*.bats`.

Test case model:

    @test "[test-REF]: description" {
        run CMD
        $OST_DEBUG && {
            echo "running: CMD"
            echo "output: $output"
            echo "status: $status"
        } >&1
        [ "$status" -eq 0 ]
    }

Command specifications are described in `DEV_NOTES.md`.
