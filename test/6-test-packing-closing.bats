#!/usr/bin/env bats
#
# <http://github.com/piwi/opensec>
# Copyright (c) 2015 Pierre Cassat & contributors
# License GNU-GPL v 3.0 - This program comes with ABSOLUTELY NO WARRANTY.
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code or see <http://www.gnu.org/licenses/>.
#
# Test for "packing" commands: compress / extract / encrypt / decrypt / open / close
#

load test-commons

setup(){
    tests_setup;
    make_opened_package;
}

teardown(){
    tests_teardown;
}

@test "[packing-1]: compression of package's data" {
    run "$OPENSECLIBEXEC/opensec" compress-data "$OST_TMP_PACKAGE"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec compress-data $OST_TMP_PACKAGE"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ -f "${OST_TMP_PACKAGE}/${DIR_DATA}${EXT_TARBALL}" ]
    [ -f "${OST_TMP_PACKAGE}/${DIR_DATA}${EXT_TARBALL}${EXT_SUM}" ]
}

@test "[packing-2]: compression of data of a non-existing package (status>0)" {
    run "$OPENSECLIBEXEC/opensec" compress-data "$OST_TMP_PACKAGEabcdefghijklmnopqrstuvw"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec compress-data $OST_TMP_PACKAGEabcdefghijklmnopqrstuvw"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -ne 0 ]
}

@test "[packing-3]: compression of package's already compressed data (status=0)" {
    make_opened_package_data_compressed;
    package="${OST_TESTDIR}/$(basename "$OST_FIXTURES_PACKAGEDIR_DATATARBALL")"
    run "$OPENSECLIBEXEC/opensec" compress-data "$package"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec compress-data $package"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
}

@test "[packing-4]: encryption of package's compressed data" {
    # compress data
    run "$OPENSECLIBEXEC/opensec" compress-data "$OST_TMP_PACKAGE"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec compress-data $OST_TMP_PACKAGE"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ -f "${OST_TMP_PACKAGE}/${DIR_DATA}${EXT_TARBALL}" ]
    [ -f "${OST_TMP_PACKAGE}/${DIR_DATA}${EXT_TARBALL}${EXT_SUM}" ]
    # encrypt data
    run "$OPENSECLIBEXEC/opensec" -f encrypt-data "$OST_TMP_PACKAGE" "$OST_PASS"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec -f encrypt-data $OST_TMP_PACKAGE $OST_PASS"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ -f "${OST_TMP_PACKAGE}/${DIR_DATA}${EXT_TARBALL}${EXT_CRYPTED}" ]
    [ ! -f "${OST_TMP_PACKAGE}/${DIR_DATA}${EXT_TARBALL}" ]
    [ -f "${OST_TMP_PACKAGE}/${DIR_DATA}${EXT_TARBALL}${EXT_SUM}" ]
}

@test "[packing-5]: encryption of package's compressed data and no FORCED mode" {
    skip 'MANUAL'
}

@test "[packing-6]: encryption of data of a non-existing package (status>0)" {
    run "$OPENSECLIBEXEC/opensec" -f encrypt-data "$OST_TMP_PACKAGEabcdefghijklmnopqrstuvw" "$OST_PASS"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec -f encrypt-data $OST_TMP_PACKAGEabcdefghijklmnopqrstuvw $OST_PASS"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -ne 0 ]
}

@test "[packing-7]: encryption of package's already encrypted data (status=0)" {
    make_opened_package_data_encrypted;
    package="${OST_TESTDIR}/$(basename "$OST_FIXTURES_PACKAGEDIR_DATAENCRYPTED")"
    run "$OPENSECLIBEXEC/opensec" -f encrypt-data "$package"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec -f encrypt-data $package"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
}

@test "[packing-8]: full close of package's data" {
    run "$OPENSECLIBEXEC/opensec" -f close-data "$OST_TMP_PACKAGE" "$OST_PASS"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec -f close-data $OST_TMP_PACKAGE $OST_PASS"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ -f "${OST_TMP_PACKAGE}/${DIR_DATA}${EXT_TARBALL}${EXT_CRYPTED}" ]
    [ ! -f "${OST_TMP_PACKAGE}/${DIR_DATA}${EXT_TARBALL}" ]
    [ -f "${OST_TMP_PACKAGE}/${DIR_DATA}${EXT_TARBALL}${EXT_SUM}" ]
}

@test "[packing-9]: full close of package's data and no FORCED mode" {
    skip 'MANUAL'
}

@test "[packing-10]: compression of package with compressed data" {
    # close data
    run "$OPENSECLIBEXEC/opensec" -f close-data "$OST_TMP_PACKAGE" "$OST_PASS"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec -f close-data $OST_TMP_PACKAGE $OST_PASS"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ -f "${OST_TMP_PACKAGE}/${DIR_DATA}${EXT_TARBALL}${EXT_CRYPTED}" ]
    [ ! -f "${OST_TMP_PACKAGE}/${DIR_DATA}${EXT_TARBALL}" ]
    [ -f "${OST_TMP_PACKAGE}/${DIR_DATA}${EXT_TARBALL}${EXT_SUM}" ]
    # compress package
    run "$OPENSECLIBEXEC/opensec" compress "$OST_TMP_PACKAGE"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec compress $OST_TMP_PACKAGE"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ -f "${OST_TMP_PACKAGE}${EXT_TARBALL}" ]
    [ -f "${OST_TMP_PACKAGE}${EXT_TARBALL}${EXT_SUM}" ]
}

@test "[packing-11]: compression of a non-existing package (status>0)" {
    run "$OPENSECLIBEXEC/opensec" compress "$OST_TMP_PACKAGEabcdefghijklmnopqrstuvw"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec compress $OST_TMP_PACKAGEabcdefghijklmnopqrstuvw"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -ne 0 ]
}

@test "[packing-12]: compression of package's already compressed data (status=0)" {
    make_closed_package_compressed;
    package="${OST_TESTDIR}/$(basename "$OST_FIXTURES_PACKAGEDIR_TARBALL")"
    run "$OPENSECLIBEXEC/opensec" compress "$package"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec compress $package"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
}

@test "[packing-13]: encryption of compressed package" {
    # close data
    run "$OPENSECLIBEXEC/opensec" -f close-data "$OST_TMP_PACKAGE" "$OST_PASS"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec -f close-data $OST_TMP_PACKAGE $OST_PASS"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ -f "${OST_TMP_PACKAGE}/${DIR_DATA}${EXT_TARBALL}${EXT_CRYPTED}" ]
    [ ! -f "${OST_TMP_PACKAGE}/${DIR_DATA}${EXT_TARBALL}" ]
    [ -f "${OST_TMP_PACKAGE}/${DIR_DATA}${EXT_TARBALL}${EXT_SUM}" ]
    # compress package
    run "$OPENSECLIBEXEC/opensec" compress "$OST_TMP_PACKAGE"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec compress $OST_TMP_PACKAGE"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ -f "${OST_TMP_PACKAGE}${EXT_TARBALL}" ]
    [ -f "${OST_TMP_PACKAGE}${EXT_TARBALL}${EXT_SUM}" ]
    # encrypt package
    run "$OPENSECLIBEXEC/opensec" encrypt "$OST_TMP_PACKAGE" "$OST_USER"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec encrypt $OST_TMP_PACKAGE $OST_USER"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ -f "${OST_TMP_PACKAGE}${EXT_TARBALL}${EXT_CRYPTED}" ]
    [ ! -f "${OST_TMP_PACKAGE}${EXT_TARBALL}" ]
    [ -f "${OST_TMP_PACKAGE}${EXT_TARBALL}${EXT_SUM}" ]
}

@test "[packing-14]: encryption of compressed package and no FORCED mode" {
    skip 'MANUAL'
}

@test "[packing-15]: encryption of a non-existing package (status>0)" {
    run "$OPENSECLIBEXEC/opensec" encrypt "$OST_TMP_PACKAGEabcdefghijklmnopqrstuvw"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec encrypt $OST_TMP_PACKAGEabcdefghijklmnopqrstuvw"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -ne 0 ]
}

@test "[packing-16]: encryption of an already encrypted package (status=0)" {
    make_closed_package_encrypted;
    package="${OST_TESTDIR}/$(basename "$OST_FIXTURES_PACKAGEDIR_ENCRYPTED")"
    run "$OPENSECLIBEXEC/opensec" -f encrypt "$package"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec -f encrypt $package"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
}

@test "[packing-17]: full close of a package" {
    # close data
    run "$OPENSECLIBEXEC/opensec" -f close-data "$OST_TMP_PACKAGE" "$OST_PASS"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec -f close-data $OST_TMP_PACKAGE $OST_PASS"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ -f "${OST_TMP_PACKAGE}/${DIR_DATA}${EXT_TARBALL}${EXT_CRYPTED}" ]
    [ ! -f "${OST_TMP_PACKAGE}/${DIR_DATA}${EXT_TARBALL}" ]
    [ -f "${OST_TMP_PACKAGE}/${DIR_DATA}${EXT_TARBALL}${EXT_SUM}" ]
    # close package
    run "$OPENSECLIBEXEC/opensec" -c close "$OST_TMP_PACKAGE" "$OST_USER"
    $OST_DEBUG && {
        echo "running: $OPENSECLIBEXEC/opensec -c close $OST_TMP_PACKAGE $OST_USER"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    [ -f "${OST_TMP_PACKAGE}${EXT_TARBALL}${EXT_CRYPTED}" ]
    [ ! -f "${OST_TMP_PACKAGE}${EXT_TARBALL}" ]
    [ -f "${OST_TMP_PACKAGE}${EXT_TARBALL}${EXT_SUM}" ]
}

@test "[packing-18]: full close of a package and no FORCED mode" {
    skip 'MANUAL'
}
