#!/usr/bin/env bats
#
# <http://github.com/piwi/opensec>
# Copyright (c) 2015 Pierre Cassat & contributors
# License GNU-GPL v 3.0 - This program comes with ABSOLUTELY NO WARRANTY.
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code or see <http://www.gnu.org/licenses/>.
#
# Setup for opensec tests: check environment
#

load test-commons

test_dir() {
    local _dir="$1"
    if [ ! -e "$_dir" ] || [ ! -d "$_dir" ]; then
        echo "> can not find the '${_dir/${OST_FIXTURESDIR}/}' directory in '${OST_FIXTURESDIR}'!" >&2
        exit 1
    fi
}

test_file() {
    local _file="$1"
    if [ ! -e "$_file" ] || [ ! -f "$_file" ]; then
        echo "> can not find the '${_file/${OST_FIXTURESDIR}/}' file in '${OST_FIXTURESDIR}'!" >&2
        exit 1
    fi
}

content_exists() {
    local _content="$1"
    local _dir="$2"
    if [ ! -e "${_dir}/${_content}" ] || [ ! -f "${_dir}/${_content}" ]; then
        echo "> can not find the '${_content}' file in '${_dir}'!" >&2
        exit 1
    fi
}

command_exists() {
    local _cmd="$1"
    if ! command -v "$_cmd"; then
        echo "> command '${_cmd}' seems to not exist in environment!" >&2
        exit 1
    fi
}

#@test "test error status of BATS" {
#    [ true = false ]
#}

@test "[setup-0]: test of setup functions (status==0)" {
    run test_dir "$(dirname "$0")"
    $OST_DEBUG && {
        echo "running: test_dir \$(dirname \$0)"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    run test_file "$0"
    $OST_DEBUG && {
        echo "running: test_file \$0"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    run content_exists "$(basename "$0")" "$(dirname "$0")"
    $OST_DEBUG && {
        echo "running: content_exists \$(basename \$0) \$(dirname \$0)"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
    run command_exists bash
    $OST_DEBUG && {
        echo "running: command_exists bash"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -eq 0 ]
}

@test "[setup-1]: test of setup functions (status>0)" {
    run test_dir "${OST_FIXTURESDIR}/abcdefghijklmnopqrstuvw"
    $OST_DEBUG && {
        echo "running: test_dir ${OST_FIXTURESDIR}/abcdefghijklmnopqrstuvw"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -ne 0 ]
    run test_file "${OST_FIXTURESDIR}/abcdefghijklmnopqrstuvw"
    $OST_DEBUG && {
        echo "running: test_file ${OST_FIXTURESDIR}/abcdefghijklmnopqrstuvw"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -ne 0 ]
    run content_exists abcdefghijklmnopqrstuvw "${OST_FIXTURESDIR}"
    $OST_DEBUG && {
        echo "running: content_exists abcdefghijklmnopqrstuvw ${OST_FIXTURESDIR}"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -ne 0 ]
    run command_exists abcdefghijklmnopqrstuvw
    $OST_DEBUG && {
        echo "running: command_exists abcdefghijklmnopqrstuvw"
        echo "output: $output"
        echo "status: $status"
    } >&1
    [ "$status" -ne 0 ]
}

@test "[setup-2]: test the test directories & files" {
    # required dirs
    declare -a dirs=()
    dirs[${#dirs[*]}]="$OST_FIXTURES_LOREMIPSUMDIR"
    dirs[${#dirs[*]}]="$OST_FIXTURES_PACKAGEDIR_OPENED"
    dirs[${#dirs[*]}]="$OST_FIXTURES_PACKAGEDIR_DATATARBALL"
    dirs[${#dirs[*]}]="$OST_FIXTURES_PACKAGEDIR_DATAENCRYPTED"
    # required files
    declare -a files=()
    files[${#files[*]}]="$OST_FIXTURES_PACKAGEDIR_TARBALL"
    files[${#files[*]}]="$OST_FIXTURES_PACKAGEDIR_TARBALLSUM"
    files[${#files[*]}]="$OST_FIXTURES_PACKAGEDIR_ENCRYPTED"
    # tests dirs
    for d in "${dirs[@]}"; do
        [ -z "$d" ] && continue;
        run test_dir "$d"
        $OST_DEBUG && {
            echo "running: test_dir $d"
            echo "output: $output"
            echo "status: $status"
        } >&1
        [ "$status" -eq 0 ]
    done
    # tests files
    for f in "${files[@]}"; do
        [ -z "$f" ] && continue;
        run test_file "$f"
        $OST_DEBUG && {
            echo "running: test_file $f"
            echo "output: $output"
            echo "status: $status"
        } >&1
        [ "$status" -eq 0 ]
    done
}

@test "[setup-3]: test the fixtures files" {
    # dirs to check
    declare -a fixturesdirs=()
    fixturesdirs[${#fixturesdirs[*]}]="$OST_FIXTURES_LOREMIPSUMDIR"
    fixturesdirs[${#fixturesdirs[*]}]="${OST_FIXTURES_PACKAGEDIR_OPENED}/data"
    # contents
    declare -a contents=()
    contents[${#contents[*]}]="$OST_CONTENTS_FILE"
    contents[${#contents[*]}]="$OST_CONTENTS_FILEBIS"
    contents[${#contents[*]}]="$OST_CONTENTS_IMAGE"
    contents[${#contents[*]}]="$OST_CONTENTS_PDF"
    contents[${#contents[*]}]="$OST_CONTENTS_ODT"
    # test
    for d in "${fixturesdirs[@]}"; do
        [ -z "$d" ] && continue;
        for c in "${contents[@]}"; do
            [ -z "$c" ] && continue;
            run content_exists "$c" "$d"
            $OST_DEBUG && {
                echo "running: content_exists $c $d"
                echo "output: $output"
                echo "status: $status"
            } >&1
            [ "$status" -eq 0 ]
        done
    done
}

@test "[setup-4]: test of required commands" {
    # commands
    declare -a requiredcmds=(
        openssl
        cat
        tar
        file
        du
        md5sum
        sed
    );
    # test
    for c in "${requiredcmds[@]}"; do
        [ -z "$c" ] && continue;
        run command_exists "$c"
        $OST_DEBUG && {
            echo "running: command_exists $c"
            echo "output: $output"
            echo "status: $status"
        } >&1
        [ "$status" -eq 0 ]
    done
}
