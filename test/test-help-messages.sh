#!/usr/bin/env bash
#
# <http://github.com/piwi/opensec>
# Copyright (c) 2015 Pierre Cassat & contributors
# License GNU-GPL v 3.0 - This program comes with ABSOLUTELY NO WARRANTY.
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code or see <http://www.gnu.org/licenses/>.
#
# Test all usage & help messages
#

OPENSECLIBEXEC="$(pwd)/libexec/"

title() {
    echo '##########################################'
    echo "# $1"
    echo '##########################################'
}

title 'global help'
$OPENSECLIBEXEC/opensec help

for f in ${OPENSECLIBEXEC}/opensec-*; do
    cmd="${f/${OPENSECLIBEXEC}\/opensec-/}"
    if [ -n "$cmd" ] && [ "$cmd" != 'core' ] && [ "$cmd" != 'help' ]; then
        title "opensec $cmd"
        $OPENSECLIBEXEC/opensec help "$cmd"
    fi
done
