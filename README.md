opensec - The data safety packager
==================================

[![Build Status](https://travis-ci.org/piwi/opensec.svg)](https://travis-ci.org/piwi/opensec)

Concept
-------

The basic concept of *opensec* is to enable users to secure some data (documents, pictures ...) 
in a compressed and encrypted tarball and still be able to use its contents for a day-to-day usage
(add, update or deleting content). The process uses a secure password and the encryption is 
performed with [*openssl*](http://www.openssl.org/), an open source toolkit 
implementing the [*Secure Sockets Layer*](http://en.wikipedia.org/wiki/Transport_Layer_Security) 
(SSL v2 and v3) and its descendant [*Transport Layer Security*](http://en.wikipedia.org/wiki/Transport_Layer_Security) (TLS) 
protocols as well as a full-strength general purpose cryptography.


How does it work?
-----------------

The idea is pretty simple: aggregate the files to secure in a first tarball encrypted with a password
and make a wrapper tarball with simple text files (not encrypted) including a log about what has been done
on the archive along the time and, eventually, a clue to retrieve the password and a description of 
the package.

The final structure of a secured tarball is something like (here for a basic package):

    [wrapper.tar.gz.enc]
    | -- clue
    | -- description
    | -- opensec.log
    | -- data.tar.gz.enc

The first encryption - for the `data.tar.gz.enc` file above - will use a custom password prompted at least
each time the archive will be opened. **You need to keep this password safe and to remind it!**

The second encryption, optional, of the whole wrapper - the `wrapper.tar.gz.enc` file above - 
will use your username by default. This way, you only need to remind your name (!).

When a tarball process (compression) is done, a *checksum* file is generated
with the hash sum of the resulting tarball; the checksum file is named like 
`PACKAGE-NAME.tar.gz.sum`. Before a tarball is extracted, the sum is compared to 
actual sum of concerned tarball if the checksum file exists.


Installation & update
---------------------

To install or update *opensec*, use the `install.sh` script:

    ./install.sh /usr/local/


Usage & Available actions
-------------------------

The global usage of the `opensec` command is:

    opensec [-c|-f|-g|-l|-v] [--crypt|--force|--global|--local|--verbose] <action> [arguments ...] --
        
To get help on an action, run:

    opensec help <action>

All `pack-*` actions will process an "all-in-one" action by fully opening
a package and its data (decrypt and extract if needed) and then closing it
after the action to rollback the package to its original state (with the 
same passwords).

### Create / Init

These actions will create or initialize a new package by preparing a directory structure.

    opensec create /home/user/path-to-package [package's description]
    opensec init [-f] /home/user/path-to-package [package's description]

### Show / Add / Edit / Replace / Remove

These actions act on files or directories inside a package. 

    opensec add /home/user/path-to-package ~/my-new-file-path 
    opensec add /home/user/path-to-package ~/my-new-file-path /my/local/file-path
    opensec pack-add /home/user/path-to-package ~/my-new-file-path 

    opensec replace /home/user/path-to-package ~/my-new-file-path /my/old-file-path-to-replace
    opensec pack-replace /home/user/path-to-package ~/my-new-file-path /my/old-file-path-to-replace

    opensec show /home/user/path-to-package /my/local-file-path-to-remove
    opensec pack-show /home/user/path-to-package /my/local-file-path-to-remove

    opensec edit /home/user/path-to-package /my/local-file-path-to-remove
    opensec pack-edit /home/user/path-to-package /my/local-file-path-to-remove

    opensec remove /home/user/path-to-package /my/local-file-path-to-remove
    opensec pack-remove /home/user/path-to-package /my/local-file-path-to-remove


### Extract / Compress

These will compress or extract a package or its data using the `tar` command. Each *compress*
process will create a checksum file with current tarball sum hash.

    opensec extract /home/user/path-to-package
    opensec compress-data /home/user/path-to-package

### Encrypt / Decrypt

These will encrypt or decrypt a package or its data using the `openssl` program and a
password prompted on terminal.

    opensec encrypt /home/user/path-to-package
    opensec decrypt-data /home/user/path-to-package

### Open / Close

These will open or close a package or its data by decrypting and extracting it locally.

    opensec open /home/user/path-to-package.tar.gz 
    opensec close-data /home/user/path-to-package.tar.gz

    opensec pack-open /home/user/path-to-package.tar.gz 
    opensec pack-close /home/user/path-to-package.tar.gz


Requirements
------------

The application requires to be run in a UNIX-like system with the following commands available:

-   [*openssl*](http://www.openssl.org/) for encryption, 
    with [AES ciphers](http://en.wikipedia.org/wiki/Advanced_Encryption_Standard)
-   the [*tar archiver* (`tar`)](http://www.gnu.org/software/tar/) GNU command 
-   the [*file types* (`file`)](http://en.wikipedia.org/wiki/File_%28command%29) UNIX command 
-   the [*disk usage* (`du`)](http://en.wikipedia.org/wiki/Du_%28Unix%29) UNIX command 
-   the [*MD5* (`md5sum` or `gmd5sum`)](http://en.wikipedia.org/wiki/Md5sum) UNIX or GNU command 
-   the [*SHA1* (`sha1sum` or `gsha1sum`)](http://en.wikipedia.org/wiki/Sha1sum) UNIX or GNU command 

GNU commands listed above can be downloaded from the [Free Software Foundation](http://my.fsf.org/) website.
UNIX commands listed above may be already installed in most of UNIX systems (including GNU/Linux and Mac OSX).


License & bug reports
---------------------

This program is developed by [Pierre Cassat](http://e-piwi.fr/) and is a free software licensed under the 
[GNU General Public License version 3.0](http://www.gnu.org/licenses/gpl.html):

-   you can redistribute it and/or modify it under the terms of the license as 
    published by the Free Software Foundation, either version 3, or (at your option) 
    any later version
-   it is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the `LICENSE` file of the package for a full copy of the license.

For sources updates and bug reports, please see the public repository of the program at
<http://github.com/piwi/opensec>.
