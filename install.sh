#!/usr/bin/env bash
#
# <http://github.com/piwi/opensec>
# Copyright (c) 2015 Pierre Cassat & contributors
# License GNU-GPL v 3.0 - This program comes with ABSOLUTELY NO WARRANTY.
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code or see <http://www.gnu.org/licenses/>.
#
# install or update opensec
# inspired by <https://github.com/sstephenson/bats>
#
set -e

resolve_link() {
    $(type -p greadlink readlink | head -1) "$1"
}

abs_dirname() {
    local cwd="$(pwd)"
    local path="$1"

    while [ -n "$path" ]; do
        cd "${path%/*}"
        local name="${path##*/}"
        path="$(resolve_link "$name" || true)"
    done

    pwd
    cd "$cwd"
}

OPENSEC_ROOT="$(abs_dirname "$0")"
PREFIX="${1%/}"
ACTION="${2:-install}"
if [ -z "$1" ]; then
    {   echo "usage: $0 <prefix> [action=install]"
        echo "  e.g. $0 /usr/local"
        echo "       $0 /usr/local cleanup"
    } >&2
    exit 1
fi

# full cleanup
rm -rf "$PREFIX"/{bin,etc,etc/bash_completion.d,libexec,share/man/man1}/opensec*
# full install
if [ "$ACTION" = 'install' ]; then
    mkdir -p "$PREFIX"/{bin,etc/bash_completion.d,libexec,share/man/man1}
    for f in "$OPENSEC_ROOT"/{bin,libexec,etc}/*; do
        path="${f/${OPENSEC_ROOT}\//}"
        cp -R "$f" "$PREFIX"/"$(dirname "$path")"/
    done
    cp -R "$OPENSEC_ROOT"/man/* "$PREFIX"/share/man/man1/
    echo "Installed OpenSec to $PREFIX/bin/opensec"
else
    echo "Removed OpenSec from $PREFIX/bin/opensec"
fi
