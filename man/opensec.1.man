.\" man: OpenSec Manual
.\" man-name: opensec
.\" author: Pierre Cassat
.\" section: 1
.\" date: 2015-08-03
.\" version: 0.2.0
.TH  "opensec" "1" "2015-08-03" "Version 0.2.0" "OpenSec Manual"
.SH NAME
.PP
OpenSec - The data safety packager
.SH SYNOPSIS
.PP
\fBopensec\fP <action> [\fBoptions\fP] [\fBarguments\fP] --
    [\fB-c\fP|\fB-f\fP|\fB-g\fP|\fB-l\fP|\fB-v\fP] [\fB--crypt\fP|\fB--force\fP|\fB--global\fP|\fB--local\fP|\fB--verbose\fP]
    \fIstatus\fP / \fIlist\fP / \fIcheck\fP <package>
    \fIcreate\fP / \fIinit\fP <package> [description]
    \fIadd\fP / \fIreplace\fP <package> <original_file> [local_path=/]
    \fIshow\fP / \fIedit\fP / \fIremove\fP <package> <local_path>
    \fIencrypt\fP / \fIdecrypt\fP / \fIencrypt-data\fP / \fIdecrypt-data\fP <package> [password]
    \fIextract\fP / \fIcompress\fP / \fIextract-data\fP / \fIcompress-data\fP <package>
    \fIopen\fP / \fIclose\fP / \fIopen-data\fP / \fIclose-data\fP <package> [password]
    \fIclue\fP / \fIdescription\fP / \fIlog\fP <package> [message]
    \fIconfig\fP <package> [var_name] [value]
    \fIsalt\fP <preset> [length]
.PP
\fBopensec\fP <pack-action> [-v|-f|-c] [\fBarguments\fP] --
    \fIpack-open\fP <package_name> [data password] [user password]
    \fIpack-close\fP <package_name> [data password] [user password]
    \fIpack-add\fP <package_name> <original_file> [local_path=/] [data password] [user password]
    \fIpack-add\fP <package_name> <local_path> [data password] [user password]
    \fIpack-remove\fP <package_name> <local_path> [data password] [user password]
    \fIpack-replace\fP <package_name> <original_file> <local_path> [data password] [user password]
    \fIpack-show\fP <package_name> <local_path> [data password] [user password]
.SH DESCRIPTION
.PP
The basic concept of \fIopensec\fP is to enable users to secure some data (documents, pictures ...) 
in a compressed and encrypted tarball and still be able to use its contents for a day-to-day usage
(add, update or deleting content). The process uses a secure password and the encryption is 
performed with \fIopenssl\fP <http://www.openssl.org/>, an open source toolkit implementing the 
\fISecure Sockets Layer\fP (SSL v2 and v3) and its descendant \fITransport Layer Security\fP (TLS) 
protocols as well as a full-strength general purpose cryptography.
.PP
The final structure of a secured tarball is something like (here for a basic package):
.RS

.EX
[wrapper.tar.gz.enc]
.br
| -- clue
.br
| -- description
.br
| -- opensec.log
.br
| -- data.tar.gz.enc
.EE
.RE
.PP
The first encryption - for the `\fSdata.tar.gz.enc\fP` file above - will use a custom password prompted 
at least each time the archive will be opened. The second encryption, optional, of the whole wrapper 
- the `\fSwrapper.tar.gz.enc\fP` file above - will use current username by default.
.PP
A package directory or data subdirectory can only be in three states:
.RS
.IP \(bu 
\fIopened\fP (not compressed neither encrypted)
.IP \(bu 
\fIcompressed\fP (not encrypted) ; tarballs may be named like `\fSPACKAGE-NAME.tar.gz\fP`
.IP \(bu 
\fIencrypted\fP (compressed and encrypted) ; encrypted tarballs may be named like
`\fSPACKAGE-NAME.tar.gz.enc\fP`.
.RE
.PP
When a tarball process (compression) is done, a \fIchecksum\fP file is generated
with the hash sum of the resulting tarball; the checksum file is named like 
`\fSPACKAGE-NAME.tar.gz.sum\fP`. Before a tarball is extracted, the sum is compared to 
actual sum of concerned tarball if the checksum file exists.
.SH OPTIONS
.PP
The following options are supported:
.TP
\fB-c\fP, \fB--crypt\fP
Use a full-encryption (data + package).
.TP
\fB-f\fP, \fB--force\fP
Force some commands to not prompt confirmation.
.TP
\fB-g\fP, \fB--global\fP
Use global configuration variables (per user).
.TP
\fB-l\fP, \fB--local\fP
Use local configuration variables (per package - default).
.TP
\fB-v\fP, \fB--verbose\fP
Increase script verbosity.
.TP
\fB-x\fP, \fB--debug\fP
See debug info.
.PP
The following internal actions are available:
.TP
\fBabout\fP
See opensec information
.TP
\fBhelp / usage\fP
See the help information about the script or an action: `\fSopensec help <action>\fP`
.TP
\fBversion\fP
See opensec version
.SH ACTIONS
.PP
Local paths are relative to the 'data/' directory of the package. They can be
written locally or globally (with the package-name) or as an absolute path
(with the package path).
.TP
\fBadd\fP
Add a new file or directory in a package defining its local name or path eventually.
.br
opensec add [--verbose] <package> <original_file> [local_path=/]
.br
.TP
\fBcheck\fP, \fBcheck-data\fP
Check integrity of a package or its data by comparing its sum to stored one (this needs
a checksum file).
.br
opensec check [--verbose] <package>
.br
opensec check-data [--verbose] <package>
.br
.TP
\fBclose\fP, \fBclose-data\fP
Close an existing package or its data (compress & encrypt it if needed).
.br
opensec close [--crypt] [--verbose] [--force] <package> [package password]
.br
opensec close-data [--verbose] [--force] <package> [data password]
.br
opensec pack-close [--crypt] [--verbose] [--force] <package> [data password] [package password]
.br
.TP
\fBclue\fP
Read or edit a package's data password clue.
.br
opensec clue [--verbose] <package> [message]
.br
opensec clue [--verbose] <package> ['edit']
.br
opensec clue [--verbose] <package> ['prompt']
.br
.TP
\fBcompress\fP, \fBcompress-data\fP
Compress an existing package or its data.
.br
opensec compress [--verbose] <package>
.br
opensec compress-data [--verbose] <package>
.br
.TP
\fBconfig\fP
Read or edit configuration locally (default) or globally. Variable names can
be written in lower or upper case and with a dot or an underscore:
.br
dir.data OR dir_data OR DIR_DATA
.br
To read actual configuration values, run:
.br
opensec config [--global] [--verbose]
.br
opensec config [--verbose] <package>
.br
To read actual single configuration value, run:
.br
opensec config [--verbose] <package> <var_name>
.br
opensec config [--global] [--verbose] <var_name>
.br
To set a configuration value, run:
.br
opensec config [--verbose] <package> <var_name> <value>
.br
opensec config [--global] [--verbose] <var_name> <value>
.br
.TP
\fBcreate\fP
Create a basic empty package with an optional description.
.br
opensec create [--verbose] <package> [description]
.br
.TP
\fBdecrypt\fP, \fBdecrypt-data\fP
Decrypt an existing package or its data with concerned password.
.br
opensec decrypt [--verbose] <package> [password]
.br
opensec decrypt-data [--verbose] <package> [password]
.br
.TP
\fBdescription\fP, \fBdesc\fP
Read or edit a package's description.
.br
opensec description [--verbose] <package> [message]
.br
opensec description [--verbose] <package> ['edit']
.br
.TP
\fBedit\fP
Edit a file of a package based on its MIME type.
.br
opensec edit [--verbose] <package> <local_path>
.br
.TP
\fBencrypt\fP, \fBencrypt-data\fP
Encrypt an existing package or its data using configured cipher and a password.
.br
opensec encrypt [--verbose] <package> [password]
.br
opensec encrypt-data [--verbose] [--force] <package> [password]
.br
.TP
\fBextract\fP, \fBextract-data\fP
Extract an existing package or its data.
.br
opensec extract [--verbose] <package>
.br
opensec extract-data [--verbose] <package>
.br
.TP
\fBinit\fP
Initialize a package in an existing directory with an optional description.
If concerned directory have initial contents, they will be added as package's
data after a confirmation. Use the \fIforced\fP mode to make that process automatically.
.br
opensec init [--verbose] [--force] <package> [description]
.br
.TP
\fBlist\fP
List a package's contents. This will render the list of contents like the `\fSls\fP` command.
.br
opensec list [--verbose] <package>
.br
.TP
\fBlog\fP
Read package's log entries or write a new entry.
.br
opensec log [--verbose] <package> [message]
.br
.TP
\fBopen\fP, \fBopen-data\fP
Open an existing package or its data (decrypt & extract it if needed).
.br
opensec open [--verbose] [--force] <package> [password]
.br
opensec open-data [--verbose] [--force] <package> [password]
.br
.TP
\fBremove\fP
Remove a file or directory from a package.
.br
opensec remove [--verbose] [--force] <package> <local_path>
.br
.TP
\fBreplace\fP
Replace a file or directory in a package.
.br
opensec replace [--verbose] <package> <original_file> [local_path=/]
.br
.TP
\fBsalt\fP
Special simple command to generate "salt" strings based on a mask of characters
with a specific length.
.br
opensec salt <preset> [length=default]
.br
.TP
\fBshow\fP
Show a file from a package based on its MIME type.
.br
opensec show [--verbose] <package> <local_path>
.br
.TP
\fBstatus\fP
Get information about a package status (\fIopened\fP, \fIcompressed\fP
and \fIencrypted\fP). Use the \fIverbose\fP mode to see data size.
.br
opensec status [--verbose] <package>
.br
.PP
All `\fSpack-*\fP` actions will process an "all-in-one" action by fully opening
a package and its data (decrypt and extract if needed) and then closing it
after the action to rollback the package to its original state (with the 
same passwords).
.TP
\fBpack-add\fP
Add a file or directory in a package in any state.
.br
opensec pack-add [-v|-f|-c] <package_name> <original_file> [local_path=/] [data password] [user password]
.br
.TP
\fBpack-close\fP
Close a package and its data.
.br
opensec pack-close [-v|-f|-c] <package_name> [data password] [user password]
.br
.TP
\fBpack-edit\fP
Edit a file in a package in any state.
.br
opensec pack-edit [-v|-f|-c] <package_name> <local_path> [data password] [user password]
.br
.TP
\fBpack-open\fP
Open a package and its data.
.br
opensec pack-edit [-v|-f] <package_name> [data password] [user password]
.br
.TP
\fBpack-remove\fP
Remove a file or directory from a package in any state.
.br
opensec pack-remove [-v|-f] <package_name> <local_path> [data password] [user password]
.br
.TP
\fBpack-replace\fP
Replace a file or directory from a package in any state.
.br
opensec pack-replace [-v|-f] <package_name> <original_file> <local_path> [data password] [user password]
.br
.TP
\fBpack-show\fP
Show a file from a package in any state.
.br
opensec pack-show [-v|-f] <package_name> <local_path> [data password] [user password]
.br
.PP
Some commands accept to read a password from the command line (for encryption and decryption). This argument
is available for convenience but \fBit is not safe to set a password as command line argument\fP, please use preferably 
the default behavior (input prompt).
.SH CONFIGURATION
.PP
The global (per user) configuration file will be stored at `\fS$HOME/.opensec\fP` and the local 
ones will be at `\fS<package-path>/.opensec\fP`. Neither is required.
.PP
Default configuration variables are:
.RS

.EX
DIR.DATA='data'
.br
FILE.LOG="opensec.log"
.br
FILE.DESCRIPTION='description'
.br
FILE.CLUE='clue'
.br
EXT.TARBALL='.tar.gz'
.br
EXT.CRYPTED='.enc'
.br
EXT.SUM='.sum'
.br
CRYPT.CIPHER='aes-256-cbc'
.br
CRYPT_SUM=APP.MD5
.br
CRYPT_SUM_ALT=APP.SHA1
.br
CHMOD.FILES=640
.br
CHMOD.DIRS=750
.br
APP.EDITOR="$EDITOR"
.br
APP.TAR="tar"
.br
APP.OPENSSL="openssl"
.br
APP.MD5="md5sum"
.br
APP.SHA1="sha1sum"
.br
APP.SED="sed"
.EE
.RE
.SH EXAMPLES
.PP
Many \fIopensec\fP subcommands require a package to work on.
In most cases, you can use a package full path or a local name:
.RS

.EX
opensec status /package/path
.br
opensec status package-name
.EE
.RE
.PP
A command that uses a local path can handle a "full" local path (with the package
path) or a "relative" path to its `\fSdata/\fP` subdirectory:
.RS

.EX
opensec show package-name /file/path/to/show/from/data
.br
opensec show package-name /tmp/package-name/file/path
.EE
.RE
.PP
Options can be used anywhere in a call:
.RS

.EX
opensec -v status package-name
.br
opensec status --verbose package-name
.br
opensec status package-name -vf
.EE
.RE
.SH LICENSE
.PP
Copyright (c) 2015, Pierre Cassat <me@e-piwi.fr> & contributors
.PP
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
.PP
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
.PP
You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
.PP
For documentation, sources & updates, see <http://github.com/piwi/opensec>. 
To read GPL-3.0 license conditions, see <http://www.gnu.org/licenses/gpl-3.0.html>.
.SH BUGS
.PP
To transmit bugs, see <http://github.com/piwi/opensec/issues>.
.SH AUTHOR
.PP
\fBOpenSec\fP is created and maintained by Pierre Cassat (piwi - <http://e-piwi.fr/>)
& contributors.
.SH SEE ALSO
.PP
bash(1), openssl(1), tar(1), md5sum(1), sha1sum(1), file(1)
