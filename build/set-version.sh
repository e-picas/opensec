#!/usr/bin/env bash
#
# <http://github.com/piwi/opensec>
# Copyright (c) 2015 Pierre Cassat & contributors
# License GNU-GPL v 3.0 - This program comes with ABSOLUTELY NO WARRANTY.
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code or see <http://www.gnu.org/licenses/>.
#
# usage: $0 <release>
#
set -e

resolve_link() {
    $(type -p greadlink readlink | head -1) "$1"
}

abs_dirname() {
    local cwd="$(pwd)"
    local path="$1"

    while [ -n "$path" ]; do
        cd "${path%/*}"
        local name="${path##*/}"
        path="$(resolve_link "$name" || true)"
    done

    pwd
    cd "$cwd"
}

OPENSEC_ROOT="$(abs_dirname "$(dirname "$0")")"
ACTUAL_VERSION="$("${OPENSEC_ROOT}/bin/opensec" version)"

echo "current version is: ${ACTUAL_VERSION}"

if [ "$#" -eq 0 ]; then
    echo "usage: $0 <version-number>"
    exit 1
fi

VERSION="$1"
DATE=$(git log -1 --format="%ci" --date=short | cut -s -f 1 -d ' ')

sed -i -e "s| APPVERSION='.*'| APPVERSION='${VERSION}'|" "${OPENSEC_ROOT}/libexec/opensec-core" \
    && sed -i -e "s|^Version: .*$|Version: ${VERSION}|;s|^Date: .*$|Date: ${DATE}|" "${OPENSEC_ROOT}/MANPAGE.md" \
    && echo "version number updated in core and manpage" \
    || echo "an error occurred!" ;
