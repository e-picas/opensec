#!/usr/bin/env bash
#
# <http://github.com/piwi/opensec>
# Copyright (c) 2015 Pierre Cassat & contributors
# License GNU-GPL v 3.0 - This program comes with ABSOLUTELY NO WARRANTY.
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code or see <http://www.gnu.org/licenses/>.
#
# generates the manpage
# usage: $0 [target=man/opensec.1.man] [source=MANPAGE.md]
#
set -e

resolve_link() {
    $(type -p greadlink readlink | head -1) "$1"
}

abs_dirname() {
    local cwd="$(pwd)"
    local path="$1"

    while [ -n "$path" ]; do
        cd "${path%/*}"
        local name="${path##*/}"
        path="$(resolve_link "$name" || true)"
    done

    pwd
    cd "$cwd"
}

OPENSEC_ROOT="$(abs_dirname "$(dirname "$0")")"
MANPATH="${1:-${OPENSEC_ROOT}/man/opensec.1.man}"
SOURCEPATH="${2:-${OPENSEC_ROOT}/MANPAGE.md}"

command -v markdown-extended >/dev/null 2>&1 || { echo "Markdown-extended command not found. Aborting." >&2; exit 1; }

markdown-extended --force -f man -o "$MANPATH" "$SOURCEPATH"
