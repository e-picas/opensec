#!/usr/bin/env bash
#
# <http://github.com/piwi/opensec>
# Copyright (c) 2015 Pierre Cassat & contributors
# License GNU-GPL v 3.0 - This program comes with ABSOLUTELY NO WARRANTY.
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code or see <http://www.gnu.org/licenses/>.
#
# usage: $0 <release>
#
set -e
set -u

resolve_link() {
    $(type -p greadlink readlink | head -1) "$1"
}

abs_dirname() {
    local cwd="$(pwd)"
    local path="$1"

    while [ -n "$path" ]; do
        cd "${path%/*}"
        local name="${path##*/}"
        path="$(resolve_link "$name" || true)"
    done

    pwd
    cd "$cwd"
}

if [ "$#" -eq 0 ]; then
    echo "usage: $0 <version-number>"
    exit 1
fi

OPENSEC_ROOT="$(abs_dirname "$(dirname "$0")")"
ACTUAL_VERSION="$("${OPENSEC_ROOT}/bin/opensec" version)"
VERSION="$1"
TAGNAME="v${VERSION}"
DATE=$(git log -1 --format="%ci" --date=short | cut -s -f 1 -d ' ')

git stash save 'pre-release stashing'
git checkout master
${OPENSEC_ROOT}/build/set-version.sh "$VERSION"
${OPENSEC_ROOT}/build/make-manpage.sh
sed -i "s/* (upcoming release)/* ${TAGNAME} (${DATE})/" CHANGELOG.md
git commit -am "Upgrade app to $VERSION (automatic commit)"
git tag "$TAGNAME" -m "New release $VERSION (automatic tag)"
git stash pop

echo "> tag v${VERSION} done, nothing pushed to remote(s)"
