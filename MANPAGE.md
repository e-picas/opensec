Man:        OpenSec Manual
Man-name:   opensec
Author:     Pierre Cassat
Section:    1
Date: 2015-08-03
Version: 0.2.0


## NAME

OpenSec - The data safety packager

## SYNOPSIS

**opensec** <action> [**options**] [**arguments**] --
    [**-c**|**-f**|**-g**|**-l**|**-v**] [**--crypt**|**--force**|**--global**|**--local**|**--verbose**]
    *status* / *list* / *check* <package>
    *create* / *init* <package> [description]
    *add* / *replace* <package> <original_file> [local_path=/]
    *show* / *edit* / *remove* <package> <local_path>
    *encrypt* / *decrypt* / *encrypt-data* / *decrypt-data* <package> [password]
    *extract* / *compress* / *extract-data* / *compress-data* <package>
    *open* / *close* / *open-data* / *close-data* <package> [password]
    *clue* / *description* / *log* <package> [message]
    *config* <package> [var_name] [value]
    *salt* <preset> [length]

**opensec** <pack-action> [-v|-f|-c] [**arguments**] --
    *pack-open* <package_name> [data password] [user password]
    *pack-close* <package_name> [data password] [user password]
    *pack-add* <package_name> <original_file> [local_path=/] [data password] [user password]
    *pack-add* <package_name> <local_path> [data password] [user password]
    *pack-remove* <package_name> <local_path> [data password] [user password]
    *pack-replace* <package_name> <original_file> <local_path> [data password] [user password]
    *pack-show* <package_name> <local_path> [data password] [user password]

## DESCRIPTION

The basic concept of *opensec* is to enable users to secure some data (documents, pictures ...) 
in a compressed and encrypted tarball and still be able to use its contents for a day-to-day usage
(add, update or deleting content). The process uses a secure password and the encryption is 
performed with [*openssl*](http://www.openssl.org/), an open source toolkit implementing the 
*Secure Sockets Layer* (SSL v2 and v3) and its descendant *Transport Layer Security* (TLS) 
protocols as well as a full-strength general purpose cryptography.

The final structure of a secured tarball is something like (here for a basic package):

    [wrapper.tar.gz.enc]
    | -- clue
    | -- description
    | -- opensec.log
    | -- data.tar.gz.enc

The first encryption - for the `data.tar.gz.enc` file above - will use a custom password prompted 
at least each time the archive will be opened. The second encryption, optional, of the whole wrapper 
- the `wrapper.tar.gz.enc` file above - will use current username by default.

A package directory or data subdirectory can only be in three states:

-   *opened* (not compressed neither encrypted)
-   *compressed* (not encrypted) ; tarballs may be named like `PACKAGE-NAME.tar.gz`
-   *encrypted* (compressed and encrypted) ; encrypted tarballs may be named like
    `PACKAGE-NAME.tar.gz.enc`.

When a tarball process (compression) is done, a *checksum* file is generated
with the hash sum of the resulting tarball; the checksum file is named like 
`PACKAGE-NAME.tar.gz.sum`. Before a tarball is extracted, the sum is compared to 
actual sum of concerned tarball if the checksum file exists.

## OPTIONS

The following options are supported:

**-c**, **--crypt**
:   Use a full-encryption (data + package).

**-f**, **--force**
:   Force some commands to not prompt confirmation. 

**-g**, **--global**
:   Use global configuration variables (per user).

**-l**, **--local**
:   Use local configuration variables (per package - default).

**-v**, **--verbose**
:   Increase script verbosity. 

**-x**, **--debug**
:   See debug info.

The following internal actions are available:

**about**
:   See opensec information

**help / usage**
:   See the help information about the script or an action: `opensec help <action>`

**version**
:   See opensec version

## ACTIONS

Local paths are relative to the 'data/' directory of the package. They can be
written locally or globally (with the package-name) or as an absolute path
(with the package path).

**add**
:   Add a new file or directory in a package defining its local name or path eventually.

        opensec add [--verbose] <package> <original_file> [local_path=/]

**check**, **check-data**
:   Check integrity of a package or its data by comparing its sum to stored one (this needs
a checksum file).

        opensec check [--verbose] <package>
        opensec check-data [--verbose] <package>

**close**, **close-data**
:   Close an existing package or its data (compress & encrypt it if needed).

        opensec close [--crypt] [--verbose] [--force] <package> [package password]
        opensec close-data [--verbose] [--force] <package> [data password]
        opensec pack-close [--crypt] [--verbose] [--force] <package> [data password] [package password]

**clue**
:   Read or edit a package's data password clue.

        opensec clue [--verbose] <package> [message]
        opensec clue [--verbose] <package> ['edit']
        opensec clue [--verbose] <package> ['prompt']

**compress**, **compress-data**
:   Compress an existing package or its data.

        opensec compress [--verbose] <package>
        opensec compress-data [--verbose] <package>

**config**
:   Read or edit configuration locally (default) or globally. Variable names can
be written in lower or upper case and with a dot or an underscore:

        dir.data OR dir_data OR DIR_DATA

:   To read actual configuration values, run:

        opensec config [--global] [--verbose]
        opensec config [--verbose] <package>

:   To read actual single configuration value, run:

        opensec config [--verbose] <package> <var_name>
        opensec config [--global] [--verbose] <var_name>

:   To set a configuration value, run:

        opensec config [--verbose] <package> <var_name> <value>
        opensec config [--global] [--verbose] <var_name> <value>

**create**
:   Create a basic empty package with an optional description.

        opensec create [--verbose] <package> [description]

**decrypt**, **decrypt-data**
:   Decrypt an existing package or its data with concerned password.

        opensec decrypt [--verbose] <package> [password]
        opensec decrypt-data [--verbose] <package> [password]

**description**, **desc**
:   Read or edit a package's description.

        opensec description [--verbose] <package> [message]
        opensec description [--verbose] <package> ['edit']

**edit**
:   Edit a file of a package based on its MIME type. 

        opensec edit [--verbose] <package> <local_path>

**encrypt**, **encrypt-data**
:   Encrypt an existing package or its data using configured cipher and a password.

        opensec encrypt [--verbose] <package> [password]
        opensec encrypt-data [--verbose] [--force] <package> [password]

**extract**, **extract-data**
:   Extract an existing package or its data.

        opensec extract [--verbose] <package>
        opensec extract-data [--verbose] <package>

**init**
:   Initialize a package in an existing directory with an optional description.
If concerned directory have initial contents, they will be added as package's
data after a confirmation. Use the *forced* mode to make that process automatically.

        opensec init [--verbose] [--force] <package> [description]

**list**
:   List a package's contents. This will render the list of contents like the `ls` command.

        opensec list [--verbose] <package>

**log**
:   Read package's log entries or write a new entry.

        opensec log [--verbose] <package> [message]

**open**, **open-data**
:   Open an existing package or its data (decrypt & extract it if needed).

        opensec open [--verbose] [--force] <package> [password]
        opensec open-data [--verbose] [--force] <package> [password]

**remove**
:   Remove a file or directory from a package.

        opensec remove [--verbose] [--force] <package> <local_path>

**replace**
:   Replace a file or directory in a package.

        opensec replace [--verbose] <package> <original_file> [local_path=/]

**salt**
:   Special simple command to generate "salt" strings based on a mask of characters
with a specific length. 

        opensec salt <preset> [length=default]

**show**
:   Show a file from a package based on its MIME type. 

        opensec show [--verbose] <package> <local_path>

**status**
:   Get information about a package status (*opened*, *compressed*
and *encrypted*). Use the *verbose* mode to see data size.

        opensec status [--verbose] <package>

All `pack-*` actions will process an "all-in-one" action by fully opening
a package and its data (decrypt and extract if needed) and then closing it
after the action to rollback the package to its original state (with the 
same passwords).

**pack-add**
:   Add a file or directory in a package in any state.

        opensec pack-add [-v|-f|-c] <package_name> <original_file> [local_path=/] [data password] [user password]

**pack-close**
:   Close a package and its data.

        opensec pack-close [-v|-f|-c] <package_name> [data password] [user password]

**pack-edit**
:   Edit a file in a package in any state.

        opensec pack-edit [-v|-f|-c] <package_name> <local_path> [data password] [user password]

**pack-open**
:   Open a package and its data.

        opensec pack-edit [-v|-f] <package_name> [data password] [user password]

**pack-remove**
:   Remove a file or directory from a package in any state.

        opensec pack-remove [-v|-f] <package_name> <local_path> [data password] [user password]

**pack-replace**
:   Replace a file or directory from a package in any state.

        opensec pack-replace [-v|-f] <package_name> <original_file> <local_path> [data password] [user password]

**pack-show**
:   Show a file from a package in any state.

        opensec pack-show [-v|-f] <package_name> <local_path> [data password] [user password]

Some commands accept to read a password from the command line (for encryption and decryption). This argument
is available for convenience but **it is not safe to set a password as command line argument**, please use preferably 
the default behavior (input prompt).

## CONFIGURATION

The global (per user) configuration file will be stored at `$HOME/.opensec` and the local 
ones will be at `<package-path>/.opensec`. Neither is required.

Default configuration variables are:

    DIR.DATA='data'
    FILE.LOG="opensec.log"
    FILE.DESCRIPTION='description'
    FILE.CLUE='clue'
    EXT.TARBALL='.tar.gz'
    EXT.CRYPTED='.enc'
    EXT.SUM='.sum'
    CRYPT.CIPHER='aes-256-cbc'
    CRYPT_SUM=APP.MD5
    CRYPT_SUM_ALT=APP.SHA1
    CHMOD.FILES=640
    CHMOD.DIRS=750
    APP.EDITOR="$EDITOR"
    APP.TAR="tar"
    APP.OPENSSL="openssl"
    APP.MD5="md5sum"
    APP.SHA1="sha1sum"
    APP.SED="sed"

## EXAMPLES

Many *opensec* subcommands require a package to work on.
In most cases, you can use a package full path or a local name:

    opensec status /package/path
    opensec status package-name

A command that uses a local path can handle a "full" local path (with the package
path) or a "relative" path to its `data/` subdirectory:

    opensec show package-name /file/path/to/show/from/data
    opensec show package-name /tmp/package-name/file/path

Options can be used anywhere in a call:

    opensec -v status package-name
    opensec status --verbose package-name
    opensec status package-name -vf

## LICENSE

Copyright (c) 2015, Pierre Cassat <me@e-piwi.fr> & contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.

For documentation, sources & updates, see <http://github.com/piwi/opensec>. 
To read GPL-3.0 license conditions, see <http://www.gnu.org/licenses/gpl-3.0.html>.

## BUGS

To transmit bugs, see <http://github.com/piwi/opensec/issues>.

## AUTHOR

**OpenSec** is created and maintained by Pierre Cassat (piwi - <http://e-piwi.fr/>)
& contributors.

## SEE ALSO

bash(1), openssl(1), tar(1), md5sum(1), sha1sum(1), file(1)
